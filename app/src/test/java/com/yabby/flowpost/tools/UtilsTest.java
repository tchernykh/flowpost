package com.yabby.flowpost.tools;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for Utils class
 */
public class UtilsTest {

    @Test
    public void testEllipsizeHtml() throws Exception {
        assertEquals("some test", Utils.ellipsizeHtml("some test", 50));
        assertEquals("SomeTest", Utils.ellipsizeHtml("SomeTest", 5));
        assertEquals("so <a href='a'> test </a>…", Utils.ellipsizeHtml("so <a href='a'> test </a> some text", 7));
        assertEquals("te <p> some…", Utils.ellipsizeHtml("te <p> some very long text here </p>", 5));
        assertEquals("te <p> some very <br/>long…", Utils.ellipsizeHtml("te <p> some very <br/>long text here </p>", 15));
        assertEquals("Test with spaces.…", Utils.ellipsizeHtml("Test with spaces.                    ", 15));
    }
}