package com.yabby.flowpost.data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test for DataTools class
 */
public class DataToolsTest {

    @Test
    public void testDataTools() {
        assertEquals("String", DataTools.fromByteArray(DataTools.toByteArray("String")));
        String[] data = (String[]) DataTools.fromByteArray(DataTools.toByteArray(new String[]{ "First", "Second"}));
        assertNotNull(data);
        assertEquals(2, data.length);
        assertEquals("First", data[0]);
        assertEquals("Second", data[1]);
    }

}