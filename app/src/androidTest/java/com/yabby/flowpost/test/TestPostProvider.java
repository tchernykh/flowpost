package com.yabby.flowpost.test;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.AndroidTestCase;

import com.yabby.flowpost.data.DataTools;
import com.yabby.flowpost.data.PostContract;
import com.yabby.flowpost.data.SortCursor;
import com.yabby.flowpost.data.facebook.FacebookContract;
import com.yabby.flowpost.data.googleplus.GooglePlusContract;
import com.yabby.flowpost.data.twitter.TwitterContract;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Test for the main PostProvider
 */
public class TestPostProvider extends AndroidTestCase {
    private static final String[][] TEST_FACEBOOK_DATA = new String[][] {
        {"clever_guy", "Albert Einshtein", "E=mc2"},
        {"clever_guy", "Albert Einshtein", "You have to learn the rules of the game. And then you have to play better than anyone else."},
        {"clever_guy", "Albert Einshtein", "Try not to become a man of success, but rather try to become a man of value."},
        {"clever_guy", "Albert Einshtein", "Look deep into nature, and then you will understand everything better."},
        {"wd", "Walt Dysney", "If you can dream it, you can do it."},
        {"wd", "Walt Dysney", "The way to get started is to quit talking and begin doing."}
    };

    private static final String[][] TEST_TWITTER_DATA = new String[][] {
        {"Dr. Seuss", "dr_seuss", "Don't cry because it's over. Smile because it happened."},
        {"Dr. Seuss", "dr_seuss", "Only you can control your future."},
        {"Dr. Seuss", "dr_seuss", "I like nonsense; it wakes up the brain cells."},
        {"Confucius", "confucius", "Life is really simple, but we insist on making it complicated."},
        {"Confucius", "confucius", "Silence is a true friend who never betrays."},
        {"Confucius", "confucius", "He who learns but does not think, is lost! He who thinks but does not learn is in great danger."}
    };

    private static final String[][] TEST_GPLUS_DATA =TEST_TWITTER_DATA;

    private Calendar yesterdayDate;
    private int facebookId = 1;
    private int twitterId = 1;
    private int gplusId = 1;
    private Map<String, Integer> facebookAuthorsMap;
    private Map<String, Integer> twitterAuthorsMap;
    private Map<String, Integer> gplusAuthorsMap;

    // brings our database to an empty state
    public void deleteAllRecords() {
        mContext.getContentResolver().delete(
                PostContract.PostEntry.CONTENT_URI,
                null,
                null
        );
        mContext.getContentResolver().delete(
                PostContract.AuthorEntry.CONTENT_URI,
                null,
                null
        );

        Cursor cursor = mContext.getContentResolver().query(
                PostContract.PostEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals(0, cursor.getCount());
        cursor.close();

        cursor = mContext.getContentResolver().query(
                PostContract.AuthorEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals(0, cursor.getCount());
        cursor.close();
    }

    // Since we want each test to start with a clean slate, run deleteAllRecords
    // in setUp (called by the test runner before each test).
    public void setUp() {
        deleteAllRecords();
        yesterdayDate = removeTime(Calendar.getInstance());
        yesterdayDate.add(Calendar.DATE, -1);
    }

    private Calendar removeTime(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    public void insertReadAuthors() {
        facebookAuthorsMap = new HashMap<>();
        twitterAuthorsMap = new HashMap<>();
        gplusAuthorsMap = new HashMap<>();

        // first we'll store author's information for facebook
        Map<String, ContentValues> facebookAuthorsValues = new HashMap<>();
        for (String[] data : TEST_FACEBOOK_DATA) {
            ContentValues cv = facebookAuthorsValues.get(data[0]);
            if (cv == null) {
                cv = new ContentValues();
                cv.put(FacebookContract.AuthorEntry.COLUMN_NETWORK_ID, data[0]);
                cv.put(FacebookContract.AuthorEntry.COLUMN_NAME, data[1]);
                facebookAuthorsValues.put(data[0], cv);
            }
        }
        ContentValues[] av = facebookAuthorsValues.values().toArray(new ContentValues[facebookAuthorsValues.values().size()]);
        mContext.getContentResolver().bulkInsert(FacebookContract.AuthorEntry.CONTENT_URI, av);
        for (ContentValues authorValue : av) {
            Integer authorId = authorValue.getAsInteger(FacebookContract.AuthorEntry._ID);
            String authorName = authorValue.getAsString(FacebookContract.AuthorEntry.COLUMN_NAME);
            assertNotNull(authorId);
            facebookAuthorsMap.put(authorName, authorId);
        }

        // and we'll store author's information for twitter
        Map<String, ContentValues> twitterAuthorsValues = new HashMap<>();
        for (String[] data : TEST_TWITTER_DATA) {
            ContentValues cv = twitterAuthorsValues.get(data[0]);
            if (cv == null) {
                cv = new ContentValues();
                cv.put(TwitterContract.AuthorEntry.COLUMN_NETWORK_ID, data[0]);
                cv.put(TwitterContract.AuthorEntry.COLUMN_NAME, data[1]);
                cv.put(TwitterContract.AuthorEntry.COLUMN_SCREEN_NAME, data[0]);
                twitterAuthorsValues.put(data[0], cv);
            }
        }
        av = twitterAuthorsValues.values().toArray(new ContentValues[twitterAuthorsValues.values().size()]);
        mContext.getContentResolver().bulkInsert(TwitterContract.AuthorEntry.CONTENT_URI, av);
        for (ContentValues authorValue : av) {
            Integer authorId = authorValue.getAsInteger(TwitterContract.AuthorEntry._ID);
            String authorName = authorValue.getAsString(TwitterContract.AuthorEntry.COLUMN_NAME);
            assertNotNull(authorId);
            twitterAuthorsMap.put(authorName, authorId);
        }

        // now it's gplus time to store author's information
        Map<String, ContentValues> gplusAuthorsValues = new HashMap<>();
        for (String[] data : TEST_GPLUS_DATA) {
            ContentValues cv = gplusAuthorsValues.get(data[0]);
            if (cv == null) {
                cv = new ContentValues();
                cv.put(GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID, data[0]);
                cv.put(GooglePlusContract.AuthorEntry.COLUMN_NAME, data[1]);
                gplusAuthorsValues.put(data[0], cv);
            }
        }
        av = gplusAuthorsValues.values().toArray(new ContentValues[gplusAuthorsValues.values().size()]);
        mContext.getContentResolver().bulkInsert(GooglePlusContract.AuthorEntry.CONTENT_URI, av);
        for (ContentValues authorValue : av) {
            Integer authorId = authorValue.getAsInteger(GooglePlusContract.AuthorEntry._ID);
            String authorName = authorValue.getAsString(GooglePlusContract.AuthorEntry.COLUMN_NAME);
            assertNotNull(authorId);
            gplusAuthorsMap.put(authorName, authorId);
        }
    }

    public void testInsertReadPosts() {
        insertReadAuthors();

        Integer facebookAuthorId = facebookAuthorsMap.get(TEST_FACEBOOK_DATA[facebookId][1]);
        Integer twitterAuthorId = twitterAuthorsMap.get(TEST_TWITTER_DATA[twitterId][1]);
        Integer gplusAuthorId = gplusAuthorsMap.get(TEST_GPLUS_DATA[gplusId][1]);
        ContentValues facebookFirstPost = getFacebookPostData(0, facebookId++, facebookAuthorId, getNextYesterdayDate());
        ContentValues twitterFirstPost = getTwitterPostData(0, twitterId++, twitterAuthorId, getNextYesterdayDate());
        ContentValues gplusFirstPost = getGplusPostData(0, gplusId++, gplusAuthorId, getNextYesterdayDate());

        Uri facebookInsertUri = mContext.getContentResolver()
                .insert(FacebookContract.PostEntry.CONTENT_URI, facebookFirstPost);

        assertTrue(facebookInsertUri != null);

        // Data's inserted.  IN THEORY.  Now pull some out to stare at it and verify it made
        // the round trip.

        // A cursor is your primary interface to the query results.
        Cursor cursor = mContext.getContentResolver().query(
                FacebookContract.PostEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        validateCursor(cursor, facebookFirstPost);

        cursor = mContext.getContentResolver().query(
                PostContract.PostEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        validateCursor(cursor, facebookFirstPost);

        Uri twitterInsertUri = mContext.getContentResolver()
                .insert(TwitterContract.PostEntry.CONTENT_URI, twitterFirstPost);
        assertTrue(twitterInsertUri != null);
        cursor = mContext.getContentResolver().query(
                TwitterContract.PostEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        validateCursor(cursor, twitterFirstPost);

        cursor = mContext.getContentResolver().query(
                PostContract.PostEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        // it should return two posts - first from twitter, second from facebook
        validateCursor(cursor, new ContentValues[] {twitterFirstPost, facebookFirstPost});

        Uri gplusInsertUri = mContext.getContentResolver()
                .insert(GooglePlusContract.PostEntry.CONTENT_URI, gplusFirstPost);
        assertTrue(gplusInsertUri != null);
        cursor = mContext.getContentResolver().query(
                GooglePlusContract.PostEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        validateCursor(cursor, gplusFirstPost);

        cursor = mContext.getContentResolver().query(
                PostContract.PostEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        // it should return three posts - first from gplus, second from facebook, third from twitter
        validateCursor(cursor, new ContentValues[] {gplusFirstPost, twitterFirstPost, facebookFirstPost});

        // next we'll insert all other our data with help of bulk insert and check that query operation
        // will fetch all our data in correct order
        // we've already added two records, so we'll skip them here
        ContentValues[] cv = new ContentValues[TEST_FACEBOOK_DATA.length + TEST_TWITTER_DATA.length + TEST_GPLUS_DATA.length];
        cv[0] = twitterFirstPost;
        cv[1] = facebookFirstPost;
        cv[2] = gplusFirstPost;
        ContentValues[] facebookValues = new ContentValues[TEST_FACEBOOK_DATA.length - 1];
        ContentValues[] twitterValues = new ContentValues[TEST_TWITTER_DATA.length - 1];
        ContentValues[] gPlusValues = new ContentValues[TEST_GPLUS_DATA.length - 1];
        int facebookDataIndex = 1;
        int twitterDataIndex = 1;
        int gPlusDataIndex = 1;
        // array should be in order - twitter - facebook - gplus - twitter - facebook ... etc
        for (int i = 3; i < cv.length; i++) {
            Date d = getNextYesterdayDate();
            switch (i % 3) {
                case 0:
                    facebookAuthorId = facebookAuthorsMap.get(TEST_FACEBOOK_DATA[facebookDataIndex][1]);
                    cv[i] = getFacebookPostData(facebookDataIndex, facebookId, facebookAuthorId, d);
                    cv[i].put(SortCursor.CURSOR_NAME_KEY, FacebookContract.FACEBOOK);
                    facebookValues[facebookDataIndex - 1] = getFacebookPostData(facebookDataIndex, facebookId, facebookAuthorId, d);
                    facebookId++;
                    facebookDataIndex++;
                    break;
                case 1:
                    twitterAuthorId = twitterAuthorsMap.get(TEST_TWITTER_DATA[twitterDataIndex][1]);
                    cv[i] = getTwitterPostData(twitterDataIndex, twitterId, twitterAuthorId, d);
                    cv[i].put(SortCursor.CURSOR_NAME_KEY, TwitterContract.TWITTER);
                    twitterValues[twitterDataIndex - 1] = getTwitterPostData(twitterDataIndex, twitterId, twitterAuthorId, d);
                    twitterId++;
                    twitterDataIndex++;
                    break;
                default:
                    gplusAuthorId = gplusAuthorsMap.get(TEST_GPLUS_DATA[gPlusDataIndex][1]);
                    cv[i] = getGplusPostData(gPlusDataIndex, gplusId, gplusAuthorId, d);
                    cv[i].put(SortCursor.CURSOR_NAME_KEY, GooglePlusContract.GOOGLEPLUS);
                    gPlusValues[gPlusDataIndex - 1] = getGplusPostData(gPlusDataIndex, gplusId, gplusAuthorId, d);
                    gplusId++;
                    gPlusDataIndex++;
                    break;
            }
        }
        Arrays.sort(cv, new Comparator<ContentValues>() {
            @Override
            public int compare(ContentValues lhs, ContentValues rhs) {
                final String ld = lhs.getAsString(PostContract.PostEntry.COLUMN_DATE);
                final String rd = rhs.getAsString(PostContract.PostEntry.COLUMN_DATE);
                return rd.compareTo(ld);
            }
        });

        mContext.getContentResolver().bulkInsert(FacebookContract.PostEntry.CONTENT_URI, facebookValues);
        mContext.getContentResolver().bulkInsert(TwitterContract.PostEntry.CONTENT_URI, twitterValues);
        mContext.getContentResolver().bulkInsert(GooglePlusContract.PostEntry.CONTENT_URI, gPlusValues);

        cursor = mContext.getContentResolver().query(
                PostContract.PostEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        validateCursor(cursor, cv);
    }

    private ContentValues getFacebookPostData(int index, int facebookId, Integer authorId, Date date) {
        ContentValues facebookPostValues = new ContentValues();
        facebookPostValues.put(FacebookContract.PostEntry.COLUMN_AUTHOR_ID, authorId);
        facebookPostValues.put(FacebookContract.PostEntry.COLUMN_CONTENT, TEST_FACEBOOK_DATA[index][2]);
        facebookPostValues.put(FacebookContract.PostEntry.COLUMN_DATE, DataTools.dateToDbString(date));
        facebookPostValues.put(FacebookContract.PostEntry.COLUMN_NETWORK_ID, facebookId);
        return facebookPostValues;
    }

    private ContentValues getTwitterPostData(int index, int twitterId, Integer authorId, Date date) {
        ContentValues twitterPostValues = new ContentValues();
        twitterPostValues.put(TwitterContract.PostEntry.COLUMN_AUTHOR_ID, authorId);
        twitterPostValues.put(TwitterContract.PostEntry.COLUMN_CONTENT, TEST_TWITTER_DATA[index][2]);
        twitterPostValues.put(TwitterContract.PostEntry.COLUMN_DATE, DataTools.dateToDbString(date));
        twitterPostValues.put(TwitterContract.PostEntry.COLUMN_NETWORK_ID, twitterId);
        return twitterPostValues;
    }

    private ContentValues getGplusPostData(int index, int gplusId, Integer authorId, Date date) {
        ContentValues gplusPostValues = new ContentValues();
        gplusPostValues.put(GooglePlusContract.PostEntry.COLUMN_AUTHOR_ID, authorId);
        gplusPostValues.put(GooglePlusContract.PostEntry.COLUMN_CONTENT, TEST_TWITTER_DATA[index][2]);
        gplusPostValues.put(GooglePlusContract.PostEntry.COLUMN_DATE, DataTools.dateToDbString(date));
        gplusPostValues.put(GooglePlusContract.PostEntry.COLUMN_NETWORK_ID, gplusId);
        return gplusPostValues;
    }

    private Date getNextYesterdayDate() {
        Date res = yesterdayDate.getTime();
        yesterdayDate.add(Calendar.HOUR_OF_DAY, 1);
        return res;
    }

    static void validateCursor(Cursor valueCursor, ContentValues expectedValues) {
        assertTrue(valueCursor.moveToFirst());
        validateRow(valueCursor, expectedValues);
        valueCursor.close();
    }

    private static void validateRow(Cursor valueCursor, ContentValues expectedValues) {
        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int idx = valueCursor.getColumnIndex(columnName);
            assertFalse(idx == -1);
            String expectedValue = entry.getValue().toString();
            assertEquals("field " + columnName + " wrong:", expectedValue, valueCursor.getString(idx));
        }
    }

    static void validateCursor(Cursor valueCursor, ContentValues[] expectedValues) {
        int index = 0;
        assertTrue(valueCursor.moveToFirst());
        do {
            validateRow(valueCursor, expectedValues[index++]);
        } while (valueCursor.moveToNext());
        valueCursor.close();
    }

}
