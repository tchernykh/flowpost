package com.yabby.flowpost;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.yabby.flowpost.data.PostContract;
import com.yabby.flowpost.networks.FacebookConnector;
import com.yabby.flowpost.networks.GooglePlusConnector;
import com.yabby.flowpost.networks.TwitterConnector;
import com.yabby.flowpost.ui.UiUtils;

/**
 * A placeholder fragment containing a simple view.
 */
public class FlowActivityFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    private final static int FLOW_LOADER = 0;

    private FlowAdapter mDataAdapter;
    private SwipeRefreshLayout mSwipeLayout;
    private boolean mFavourite = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(FLOW_LOADER, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_flow, container, false);
        RecyclerView mPostsList = (RecyclerView) rootView.findViewById(R.id.post_list);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mPostsList.setLayoutManager(mLayoutManager);

        mDataAdapter = new FlowAdapter(getActivity());
        mPostsList.setAdapter(mDataAdapter);

        mPostsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            boolean hideToolBar = false;
            boolean hidden = getFlowActivity().getToolbar().getY() < 0;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!hideToolBar && hidden) {
                    // show
                    getFlowActivity().showToolbar();
                    hidden = false;
                } else if (hideToolBar && !hidden) {
                    // hide
                    hidden = true;
                    getFlowActivity().hideToolbar();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > UiUtils.dpToPx(8)) {
                    hideToolBar = true;
                } else if (dy < -UiUtils.dpToPx(4)) {
                    hideToolBar = false;
                }
            }
        });

        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeResources(R.color.primary, R.color.accent, R.color.action);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.filter_options, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                mFavourite = !mFavourite;
                item.setIcon(mFavourite ? R.drawable.sel_ic_favourite_checked : R.drawable.sel_ic_favourite);
                getLoaderManager().restartLoader(FLOW_LOADER, null, this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        return new CursorLoader(
                getActivity(),
                PostContract.PostEntry.CONTENT_URI,
                // we need all columns returned by ContentProvider because what columns are
                // returned depends on networks and adapter knows how to show these data
                null,
                mFavourite? (PostContract.PostEntry.TABLE_NAME + "." + PostContract.PostEntry.COLUMN_FAVORITE + " = 1") : null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mDataAdapter.swapCursor(cursor);
        mSwipeLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mDataAdapter.swapCursor(null);
    }

    @Override
    public void onRefresh() {
        FacebookConnector.getInstance().syncImmediately(getActivity());
        GooglePlusConnector.getInstance().syncImmediately(getActivity());
        TwitterConnector.getInstance().syncImmediately(getActivity());
    }
}
