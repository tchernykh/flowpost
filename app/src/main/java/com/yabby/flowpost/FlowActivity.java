package com.yabby.flowpost;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import com.squareup.otto.Subscribe;
import com.yabby.flowpost.events.ViewPostEvent;
import com.yabby.flowpost.ui.UiUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;


public class FlowActivity extends AppCompatActivity {

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.container) FrameLayout mMainContainer;

    private final ValueAnimator.AnimatorUpdateListener toolbarHeightListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            Float translationY = (Float) animation.getAnimatedValue("translationY");
            ((ViewGroup.MarginLayoutParams) mMainContainer.getLayoutParams()).topMargin = toolbar.getHeight() + translationY.intValue();
            mMainContainer.requestLayout();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupTranslucency();
        setContentView(R.layout.activity_flow);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new FlowActivityFragment())
                .commit();
        }
        setSupportActionBar(toolbar);
        int barHeight = UiUtils.getStatusBarHeight(this);
        if (barHeight >= 0) {
            toolbar.setPadding(0, barHeight, 0, 0);
            ((ViewGroup.MarginLayoutParams)mMainContainer.getLayoutParams()).topMargin = UiUtils.getToolbarHeight(this);
        }
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowPostApplication.bus().register(this);
    }

    @Override
    protected void onPause() {
        FlowPostApplication.bus().unregister(this);
        super.onPause();
    }

    private void setupTranslucency() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.flow, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_configure:
                transactTo(ConfigureNetworksFragment.newInstance());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Subscribe
    public void onViewPostEvent(ViewPostEvent event) {
        transactTo(ViewPostFragment.newInstance(event.getNetworkName(), event.getPostId()));
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void showToolbar() {
        if (toolbar.getY() < 0) {
            ObjectAnimator btnAnimator = ObjectAnimator.ofFloat(toolbar, "translationY", toolbar.getTranslationY(), 0);
            btnAnimator.setInterpolator(new DecelerateInterpolator());
            btnAnimator.addUpdateListener(toolbarHeightListener);
            btnAnimator.start();
        }
    }

    public void hideToolbar() {
        if (toolbar.getY() >= 0) {
            ObjectAnimator btnAnimator = ObjectAnimator.ofFloat(toolbar, "translationY", toolbar.getTranslationY(), -toolbar.getHeight());
            btnAnimator.setInterpolator(new DecelerateInterpolator());
            btnAnimator.addUpdateListener(toolbarHeightListener);
            btnAnimator.start();
        }
    }

    public void transactTo(BaseFragment fragment) {
        Timber.d("transact to %s", fragment.getCustomTag());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(fragment.getCustomTag())
                .commit();
    }
}
