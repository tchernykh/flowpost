package com.yabby.flowpost.tools;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

import timber.log.Timber;

/**
 * Provide queue based way to download files.
 * Created by chernykh on 10/20/2014.
 */
public class DownloadManager {
    private static DownloadManager INSTANCE;

    public static DownloadManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DownloadManager();
        }
        return INSTANCE;
    }

    private Queue<String[]> mFilesToDownloadQueue;
    private Map<String, LoadHandler> handlers;
    private boolean mIsDownloading = false;

    private DownloadManager() {
        mFilesToDownloadQueue = new ConcurrentLinkedQueue<>();
        handlers = new ConcurrentHashMap<>();
    }

    public void downloadFile(String fileToDownloadUrl, String filePathToSave) {
        mFilesToDownloadQueue.add(new String[]{fileToDownloadUrl, filePathToSave});
        if (!mIsDownloading) {
            startDownload();
        }
    }

    private void startDownload() {
        mIsDownloading = true;
        new Thread() {
            @Override
            public void run() {
                while (!mFilesToDownloadQueue.isEmpty()) {
                    String[] next = mFilesToDownloadQueue.poll();
                    actualDownload(next[0], next[1]);
                    LoadHandler loadHandler = handlers.remove(next[1]);
                    Timber.d("Download handler for %s - %s ", next[0], loadHandler);
                    if (loadHandler != null) {
                        loadHandler.loaded(next[1]);
                    }
                }
                mIsDownloading = false;
            }
        }.start();
    }

    private void actualDownload(String fileToDownloadUrl, String filePathToSave) {
        Timber.d("Actual Download file %s to %s", fileToDownloadUrl, filePathToSave);

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(fileToDownloadUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // TODO handle server error
                return;
            }

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(filePathToSave);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            Timber.e(e, "Unable to download file %s to %s", fileToDownloadUrl, filePathToSave);
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
    }

    public void waitForLoading(String imagePath, LoadHandler loadHandler) {
        for (String[] fileInQueue : mFilesToDownloadQueue) {
            if (imagePath.equals(fileInQueue[1])) {
                handlers.put(imagePath, loadHandler);
                return;
            }
        }
        loadHandler.loaded(imagePath);
    }

    public String waitForLoadingSync(String imagePath) {
        final CountDownLatch doneSignal = new CountDownLatch(1);
        final String[] res = new String[1];
        waitForLoading(imagePath, new LoadHandler() {
            @Override
            public void loaded(String filePath) {
                doneSignal.countDown();
                res[0] = filePath;
            }
        });
        try {
            doneSignal.await();
        } catch (InterruptedException e) {
            // ignore
        }
        return res[0];
    }

    public static interface LoadHandler {
        void loaded(String filePath);
    }
}
