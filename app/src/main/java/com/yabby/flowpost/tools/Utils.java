package com.yabby.flowpost.tools;

import android.content.Context;
import android.net.wifi.WifiManager;

import com.yabby.flowpost.FlowPostApplication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Tools static methods through the app.
 * Created by chernykh on 9/23/2014.
 */
public final class Utils {

    public static boolean isNetworkAccessible() {
        return ((WifiManager) FlowPostApplication.getAppContext().getSystemService(Context.WIFI_SERVICE)).isWifiEnabled();
    }

    public static void safeClose(InputStream stream) {
        if(stream != null) {
            try {
                stream.close();
            } catch (IOException var2) {
                // ignore
            }
        }
    }

    public static void safeClose(OutputStream stream) {
        if(stream != null) {
            try {
                stream.close();
            } catch (IOException var2) {
                // ignore
            }
        }
    }

    public static String ellipsizeHtml(String htmlContent, int contentMaxLength) throws Exception {
        if (htmlContent.length() <= contentMaxLength) return htmlContent;
        int ind = 0;
        int count = 0;
        boolean isNonBreakingTag = false;
        outer:
        do {
            char c = htmlContent.charAt(ind);
            if (c == '<') {
                char nextChar = htmlContent.charAt(ind);
                if (nextChar == '/') {
                    // closing tag - look for end of it
                    while(htmlContent.length() > ind && htmlContent.charAt(ind) != '>') ind++;
                    isNonBreakingTag = false;
                } else {
                    // look for start of tag name
                    while(htmlContent.length() > ind && !Character.isLetter(htmlContent.charAt(ind))) {
                        if (htmlContent.charAt(ind) == '/') {
                            // closing tag - look for end of it
                            while(htmlContent.length() > ind && htmlContent.charAt(ind) != '>') ind++;
                            ind++;
                            isNonBreakingTag = false;
                            continue outer;
                        } else {
                            ind++;
                        }
                    }
                    // non breaking are <a> and <img> tags
                    isNonBreakingTag = (htmlContent.charAt(ind) == 'a' || (htmlContent.charAt(ind) == 'i' &&
                            htmlContent.charAt(ind + 1) == 'm' && htmlContent.charAt(ind + 2) == 'g'));
                    // closing tag - look for end of it
                    while(htmlContent.length() > ind && htmlContent.charAt(ind) != '>') ind++;
                }
                ind++;
                continue;
            }
            count++;
            if (count > contentMaxLength && !isNonBreakingTag && !Character.isLetterOrDigit(htmlContent.charAt(ind))) {
                // trim
                while (ind >= 0 && Character.isSpaceChar(htmlContent.charAt(ind))) ind--;
                return htmlContent.substring(0, ind + 1) + "…";
            }
            ind++;
        } while (htmlContent.length() > ind);
        return htmlContent;
    }

    public static String normalizeUrl(String url) {
        if (url.startsWith("http")) {
            return url;
        }
        return "https:" + url;
    }
}
