package com.yabby.flowpost.tools;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Manage resources like files - download, provide local urls, etc
 * Created by chernykh on 10/20/2014.
 */
public class ResourcesManager {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static ResourcesManager INSTANCE;

    public static ResourcesManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ResourcesManager();
        }
        return INSTANCE;
    }

    private ResourcesManager() {

    }

    public String downloadFile(Context context, String fileToDownloadUrl, String network, String fileType, String fileName) throws IOException {
        if (fileToDownloadUrl == null) {
            throw new IllegalArgumentException("Unable to download null url");
        }
        // files should be saved by path:
        // /FlowPost/{date}/{network}/{fileType}/{fileName}
        File file = new File(context.getFilesDir().getPath() + File.separator +
                dateFormat.format(new Date()) + File.separator +
                network + File.separator +
                fileType);
        boolean isCreated = file.mkdirs();
        if (!isCreated && !file.isDirectory()) {
            throw new IOException("Unable to make dirs: " + file.getPath());
        }
        File fileToSave = new File(file, fileName);
        if (!fileToSave.exists()) {
            DownloadManager.getInstance().downloadFile(fileToDownloadUrl, fileToSave.getPath());
        }
        return fileToSave.getPath();
    }
}
