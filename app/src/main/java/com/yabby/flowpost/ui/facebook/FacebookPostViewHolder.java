package com.yabby.flowpost.ui.facebook;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.yabby.flowpost.R;
import com.yabby.flowpost.data.PostContract;
import com.yabby.flowpost.data.facebook.FacebookContract;
import com.yabby.flowpost.ui.DefaultPostViewHolder;
import com.yabby.flowpost.ui.UiUtils;

import butterknife.Bind;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * View presenter for Facebook post
 */
public class FacebookPostViewHolder extends DefaultPostViewHolder {
    public static final int LAYOUT = R.layout.post_item_facebook;

    @Bind(R.id.facebook_post_author_image) ImageView mAuthorImage;
    @Bind(R.id.facebook_post_content_more_big) LinearLayout mPostContentMoreBig;
    @Bind(R.id.facebook_post_content_more_small) LinearLayout mPostContentMoreSmall;
    @Bind(R.id.facebook_post_name_big) TextView mPostNameBig;
    @Bind(R.id.facebook_post_name_small) TextView mPostNameSmall;
    @Bind(R.id.facebook_post_picture_big) ImageView mPostContentPictureBig;
    @Bind(R.id.facebook_post_picture_small) ImageView mPostContentPictureSmall;
    // action buttons
    @Bind(R.id.facebook_post_likes_count) TextView mPostLikesCount;
    @Bind(R.id.facebook_post_comments_count) TextView mPostCommentsCount;
    @Bind(R.id.facebook_post_shares_count) TextView mPostSharesCount;

    public FacebookPostViewHolder(View view) {
        super(view);
        mPostNameSmall.setMovementMethod(LinkMovementMethod.getInstance());
        mPostNameBig.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void setData(Cursor cursor) {
        super.setData(cursor);
        final String authorString = cursor.getString(cursor.getColumnIndex(PostContract.AuthorEntry.COLUMN_NAME));
        final String authorImageFile = cursor.getString(cursor.getColumnIndex(FacebookContract.AuthorEntry.COLUMN_IMAGE_BIG));
        final String picture = cursor.getString(cursor.getColumnIndex(FacebookContract.PostEntry.COLUMN_PICTURE));
        final String type = cursor.getString(cursor.getColumnIndex(FacebookContract.PostEntry.COLUMN_TYPE));
        final int shareCount = cursor.getInt(cursor.getColumnIndex(FacebookContract.PostEntry.COLUMN_SHARE_COUNT));
        final int commentCount = cursor.getInt(cursor.getColumnIndex(FacebookContract.PostEntry.COLUMN_COMMENTS_COUNT));
        final int likesCount = cursor.getInt(cursor.getColumnIndex(FacebookContract.PostEntry.COLUMN_LIKES_COUNT));

        Timber.d("FacebookPost: type: %s, counts: %d %d %d", type, shareCount, commentCount, likesCount);

        Glide.with(itemView.getContext()).load(authorImageFile).into(mAuthorImage);

        mPostLikesCount.setText(String.valueOf(likesCount));
        boolean favorite = cursor.getInt(cursor.getColumnIndex(FacebookContract.PostEntry.COLUMN_FAVORITE)) != 0;
        mPostLikesCount.setSelected(favorite);
        mPostCommentsCount.setText(String.valueOf(commentCount));
        mPostSharesCount.setText(String.valueOf(shareCount));

        mAuthorView.setText(authorString);

//        if (facebookPost.isShareLinkType()) {
//            name = Html.fromHtml("<a href='" + facebookPost.getLink() + "'>" + name + "</a>");
//        }
        if (picture != null) {
            final Target target = new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap bm, GlideAnimation<? super Bitmap> glideAnimation) {
                    itemView.setTag(R.id.tag_target, null);
                    if (bm != null) {
                        Timber.d("show image %d,%d on screen %d,%d", bm.getWidth(), bm.getHeight(), UiUtils.getScreenWidth(), UiUtils.getScreenHeight());
                    }
                    if (bm != null && (bm.getWidth() > (UiUtils.getScreenWidth() / 3) ||
                            (bm.getWidth() < (bm.getHeight() * 2 / 3)))) {
                        mPostContentMoreBig.setVisibility(View.VISIBLE);
                        mPostContentMoreSmall.setVisibility(View.GONE);
                        mPostContentPictureBig.setImageBitmap(bm);
                    } else {
                        mPostContentMoreSmall.setVisibility(View.VISIBLE);
                        mPostContentMoreBig.setVisibility(View.GONE);
                        mPostContentPictureSmall.setImageBitmap(bm);
                    }
                }
            };
            itemView.setTag(R.id.tag_target, target);
            Glide.with(itemView.getContext()).load(picture).into(target);
        } else {
            mPostContentMoreBig.setVisibility(View.GONE);
            mPostContentMoreSmall.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.facebook_post_likes_count)
    public void onLikeClick() {
        mPostLikesCount.setEnabled(false);
        new MarkAsFavoriteAsyncTask(FacebookContract.PostEntry.CONTENT_URI, mPostLikesCount, getPostId()).execute(!mPostLikesCount.isSelected());
    }

//
//    private CharSequence getAuthorLine(String authorString, String type, FacebookPost facebookPost) {
//        CharSequence story = facebookPost.getStory();
//        if (story != null) {
//            SpannableString formattedStory = new SpannableString(story);
//            FacebookPost.StoryTag[] tags = facebookPost.getStoryTags();
//            if (tags != null) {
//                for (FacebookPost.StoryTag tag : tags) {
//                    formattedStory.setSpan(new StyleSpan(Typeface.BOLD), tag.offset, tag.offset + tag.length, 0);
//                }
//                story = formattedStory;
//            }
//        } else if (FacebookPost.TYPE_SHARE_LINK.equals(type)) {
//            story = Html.fromHtml(authorString + " <small><font color='gray'>shared a link</font></small>");
//        }
//        return story;
//    }
}