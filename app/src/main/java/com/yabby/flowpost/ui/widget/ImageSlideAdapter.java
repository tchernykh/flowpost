package com.yabby.flowpost.ui.widget;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.yabby.flowpost.widget.ScaleImageView;

import java.util.List;

public class ImageSlideAdapter extends PagerAdapter {
    private Context mContext;
    private List<String> mImages;

	public ImageSlideAdapter(Context context, List<String> images) {
        mContext = context;
        mImages = images;
	}

    @Override
	public int getCount() {
		return mImages.size();
	}

	@Override
	public View instantiateItem(ViewGroup container, final int position) {
        ImageView imageView = new ScaleImageView(mContext);
		Glide.with(imageView.getContext()).load(mImages.get(position)).into(imageView);
        container.addView(imageView, 0);
        return imageView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
}