package com.yabby.flowpost.ui.googleplus;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yabby.flowpost.FlowPostApplication;
import com.yabby.flowpost.R;
import com.yabby.flowpost.data.PostContract;
import com.yabby.flowpost.data.googleplus.GooglePlusContract;
import com.yabby.flowpost.tools.Utils;
import com.yabby.flowpost.ui.DefaultPostViewHolder;
import com.yabby.flowpost.ui.PostViewHolder;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;
import timber.log.Timber;

/**
* Created by chernykh on 10/21/2014.
*/
public class GoogleplusPostViewHolder extends PostViewHolder {

    public static final int LAYOUT = R.layout.post_item_googleplus;
    private static final int CONTENT_MAX_LENGTH = 200;
    private static final Map<String, Integer> COLUMN_INDICES = new HashMap<>();

    private static final OnClickListener sYoutubeClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String link = (String) v.getTag(R.id.tag_youtube_link);
            if (link != null) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                FlowPostApplication.getAppContext().startActivity(intent);
            }
        }
    };

    private static int getColumnIndex(Cursor cursor, String columnName) {
        Integer res = COLUMN_INDICES.get(columnName);
        if (res == null) {
            res = cursor.getColumnIndex(columnName);
            COLUMN_INDICES.put(columnName, res);
        }
        return res;
    }

    @Bind(R.id.gplus_post_date) TextView mDateView;
    @Bind(R.id.gplus_post_content) TextView mContentView;
    @Bind(R.id.gplus_post_author) TextView mAuthorView;
    @Bind(R.id.gplus_post_author_image) ImageView mAuthorImage;
    @Bind(R.id.gplus_post_picture) ImageView mPostImage;
    @Bind(R.id.gplus_post_likes_count) TextView mPostLikesCount;
    @Bind(R.id.gplus_post_shares_count) TextView mPostSharesCount;
    @Bind(R.id.gplus_post_comments_count) TextView mPostCommentsCount;
    @Bind(R.id.gplus_post_youtube_icon) ImageView mYoutubeIcon;

    private boolean mFull;

    public GoogleplusPostViewHolder(View view, boolean full) {
        super(view);
        mFull = full;
    }

    public void setData(Cursor cursor) {
        super.setData(cursor);

        setPostData(itemView.getContext(), getPostData(cursor, mFull), mAuthorImage, mYoutubeIcon, mAuthorView, mDateView, mContentView, mPostImage);

        mPostLikesCount.setText(cursor.getString(getColumnIndex(cursor, GooglePlusContract.PostEntry.COLUMN_LIKES_COUNT)));
        boolean favorite = cursor.getInt(getColumnIndex(cursor, GooglePlusContract.PostEntry.COLUMN_FAVORITE)) != 0;
        mPostLikesCount.setSelected(favorite);
        mPostCommentsCount.setText(cursor.getString(getColumnIndex(cursor, GooglePlusContract.PostEntry.COLUMN_COMMENTS_COUNT)));
        mPostSharesCount.setText(cursor.getString(getColumnIndex(cursor, GooglePlusContract.PostEntry.COLUMN_SHARE_COUNT)));
    }

    @OnClick(R.id.gplus_post_likes_count)
    public void onLikeClick() {
        mPostLikesCount.setEnabled(false);
        new DefaultPostViewHolder.MarkAsFavoriteAsyncTask(GooglePlusContract.PostEntry.CONTENT_URI, mPostLikesCount, getPostId()).execute(!mPostLikesCount.isSelected());
    }

    private static PostData getPostData(Cursor cursor, boolean full) {
        PostData postData = new PostData();
        postData.authorImageFile = cursor.getString(getColumnIndex(cursor, PostContract.AuthorEntry.COLUMN_IMAGE_BIG));
        postData.postImageFile = cursor.getString(getColumnIndex(cursor, GooglePlusContract.PostEntry.COLUMN_CONTENT_PICTURE));
        postData.youtubeLink = cursor.getString(getColumnIndex(cursor, GooglePlusContract.PostEntry.COLUMN_CONTENT_YOUTUBE));
        postData.authorName = cursor.getString(getColumnIndex(cursor, PostContract.AuthorEntry.COLUMN_NAME));
        postData.date = cursor.getString(getColumnIndex(cursor, PostContract.PostEntry.COLUMN_DATE));
        String htmlContent = cursor.getString(getColumnIndex(cursor, PostContract.PostEntry.COLUMN_CONTENT));
        if (!full) {
            try {
                htmlContent = Utils.ellipsizeHtml(htmlContent, CONTENT_MAX_LENGTH);
            } catch (Exception e) {
                htmlContent = htmlContent.substring(0, CONTENT_MAX_LENGTH);
                Timber.e(e, "Error while Utils.ellipsizeHtml for html: %s", htmlContent);
            }
        }
        postData.contentHtml = Html.fromHtml(htmlContent);
        return postData;
    }

    /**
     * Used to set data for main and reshared parts
     */
    private static void setPostData(Context context, PostData postData, ImageView authorImage, ImageView youtubeIcon,
            TextView authorView, TextView dateView, TextView contentView, ImageView postImage) {
        Glide.with(context).load(postData.authorImageFile).into(authorImage);
        authorView.setText(postData.authorName);
        dateView.setText(postData.date);
        contentView.setText(postData.contentHtml);
        if (postData.postImageFile == null) {
            postImage.setVisibility(View.GONE);
        } else {
            postImage.setVisibility(View.VISIBLE);
            postImage.setImageDrawable(null);
            Glide.with(context).load(postData.postImageFile).crossFade().into(postImage);
        }
        if (postData.youtubeLink == null) {
            youtubeIcon.setVisibility(View.GONE);
            postImage.setTag(R.id.tag_youtube_link, null);
            postImage.setOnClickListener(null);
        } else {
            youtubeIcon.setVisibility(View.VISIBLE);
            postImage.setTag(R.id.tag_youtube_link, postData.youtubeLink);
            postImage.setOnClickListener(sYoutubeClickListener);
        }
    }

    private static class PostData {
        String authorImageFile;
        String postImageFile;
        String authorName;
        String date;
        String youtubeLink;
        Spanned contentHtml;
    }
}
