package com.yabby.flowpost.ui;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;

import com.yabby.flowpost.FlowPostApplication;
import com.yabby.flowpost.R;

/**
 * Tools methods for ui
 */
public class UiUtils {

    private static int height = -1;
    private static int width = -1;
    private static float density;
    private static float scaledDensity;
    private static int densityDpi;

    static {
        try {
            final DisplayMetrics displayMetrics = FlowPostApplication.getAppContext().getResources().getDisplayMetrics();
            density = displayMetrics.density;
            scaledDensity = displayMetrics.scaledDensity;
            densityDpi = displayMetrics.densityDpi;
            height = displayMetrics.heightPixels;
            width = displayMetrics.widthPixels;
        } catch (Exception e) {
            density = 1;
            scaledDensity = 1;
            densityDpi = DisplayMetrics.DENSITY_DEFAULT;
            height = -1;
            width = -1;
        }
    }

    public static int getScreenHeight() {
        return height;
    }

    public static int getScreenWidth() {
        return width;
    }

    public static int getMaxScreenSize() {
        return Math.max(getScreenHeight(), getScreenWidth());
    }

    public static int spToPx(float sp) {
        return (int) (sp * scaledDensity);
    }

    public static int dpToPx(float dp) {
        return (int) (dp * density);
    }

    public static int getStatusBarHeight(Context c) {
        int result = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int resourceId = c.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = c.getResources().getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    public static int getNavigationBarHeight(Context c) {
        int result = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int resourceId = c.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = c.getResources().getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    public static int getToolbarHeight(Context c) {
        return getStatusBarHeight(c) + c.getResources().getDimensionPixelSize(R.dimen.toolbar_height);
    }
}
