package com.yabby.flowpost.ui;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.yabby.flowpost.data.PostContract;
import com.yabby.flowpost.data.SortCursor;

import butterknife.ButterKnife;

/**
* Created by chernykh on 10/21/2014.
*/
public abstract class PostViewHolder extends RecyclerView.ViewHolder{

    private long mPostId;

    private String mNetworkName;

    public PostViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public long getPostId() {
        return mPostId;
    }

    public String getNetworkName() {
        return mNetworkName;
    }

    public void setNetworkName(String mNetworkName) {
        this.mNetworkName = mNetworkName;
    }

    public void setData(Cursor cursor) {
        mPostId = cursor.getLong(cursor.getColumnIndex(PostContract.PostEntry._ID));
        if (mNetworkName == null) {
            mNetworkName = cursor.getString(SortCursor.CURSOR_NAME_INDEX);
        }
    }
}
