package com.yabby.flowpost.ui;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.yabby.flowpost.R;
import com.yabby.flowpost.data.facebook.FacebookContract;
import com.yabby.flowpost.data.googleplus.GooglePlusContract;
import com.yabby.flowpost.data.twitter.TwitterContract;
import com.yabby.flowpost.ui.facebook.FacebookPostViewHolder;
import com.yabby.flowpost.ui.googleplus.GoogleplusPostViewHolder;
import com.yabby.flowpost.ui.twitter.TwitterPostViewHolder;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
* Created by chernykh on 10/21/2014.
*/
public class PostViewHolderFactory {

    @IntDef({VIEW_TYPE_FACEBOOK, VIEW_TYPE_TWITTER, VIEW_TYPE_GOOGLEPLUS})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ViewType {}

    private static final int VIEW_TYPE_FACEBOOK = 1;
    private static final int VIEW_TYPE_TWITTER = 2;
    private static final int VIEW_TYPE_GOOGLEPLUS = 3;

    public static View createViewAndSetData(String networkName, Context context, Cursor cursor) {
        @ViewType int viewType = PostViewHolderFactory.getViewType(networkName);
        PostViewHolder holder = createPostViewWithHolderFullView(viewType, context);
        holder.setNetworkName(networkName);
        holder.setData(cursor);
        return holder.itemView;
    }

    public static @NonNull PostViewHolder createPostViewWithHolder(@ViewType int viewType, Context context) {
        LinearLayout mainView = new LinearLayout(context);
        mainView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mainView.setOrientation(LinearLayout.VERTICAL);

        final View view;
        switch (viewType) {
            case VIEW_TYPE_TWITTER:
                view = LayoutInflater.from(context).inflate(TwitterPostViewHolder.LAYOUT, mainView);
                return new TwitterPostViewHolder(wrapShort(context, view));
            case VIEW_TYPE_FACEBOOK:
                view = LayoutInflater.from(context).inflate(FacebookPostViewHolder.LAYOUT, mainView);
                return new FacebookPostViewHolder(wrapShort(context, view));
            case VIEW_TYPE_GOOGLEPLUS:
                view = LayoutInflater.from(context).inflate(GoogleplusPostViewHolder.LAYOUT, mainView);
                return new GoogleplusPostViewHolder(wrapShort(context, view), false);
            default:
                throw new IllegalArgumentException("Unknown view type: " + viewType);
        }
    }

    public static @NonNull PostViewHolder createPostViewWithHolderFullView(@ViewType int viewType, Context context) {
        LinearLayout mainView = new LinearLayout(context);
        mainView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mainView.setPadding(0, 0, 0, UiUtils.getNavigationBarHeight(context));
        mainView.setOrientation(LinearLayout.VERTICAL);
        ScrollView scrollView = new ScrollView(context);
        scrollView.setLayoutParams(new ScrollView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        scrollView.addView(mainView);

        switch (viewType) {
            case VIEW_TYPE_TWITTER:
                LayoutInflater.from(context).inflate(TwitterPostViewHolder.LAYOUT, mainView);
                return new TwitterPostViewHolder(scrollView);
            case VIEW_TYPE_FACEBOOK:
                LayoutInflater.from(context).inflate(FacebookPostViewHolder.LAYOUT, mainView);
                return new FacebookPostViewHolder(scrollView);
            case VIEW_TYPE_GOOGLEPLUS:
                LayoutInflater.from(context).inflate(GoogleplusPostViewHolder.LAYOUT, mainView);
                return new GoogleplusPostViewHolder(scrollView, true);
            default:
                throw new IllegalArgumentException("Unknown view type: " + viewType);
        }
    }

    private static CardView wrapShort(Context context, View view) {
        CardView wrapperView = new CardView(context);
        final RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int m = context.getResources().getDimensionPixelSize(R.dimen.card_view_margin);
        params.setMargins(m,m,m,m);
        wrapperView.setLayoutParams(params);
        wrapperView.setRadius(context.getResources().getDimension(R.dimen.card_view_radius));
        wrapperView.addView(view);
        return wrapperView;
    }

    public static @ViewType int getViewType(String type) {
        switch (type) {
            case FacebookContract.FACEBOOK:
                return VIEW_TYPE_FACEBOOK;
            case TwitterContract.TWITTER:
                return VIEW_TYPE_TWITTER;
            case GooglePlusContract.GOOGLEPLUS:
                return VIEW_TYPE_GOOGLEPLUS;
            default:
                throw new IllegalArgumentException("Unknown view type: " + type);
        }
    }
}
