package com.yabby.flowpost.ui.twitter;

import android.database.Cursor;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.viewpagerindicator.CirclePageIndicator;
import com.yabby.flowpost.R;
import com.yabby.flowpost.data.DataTools;
import com.yabby.flowpost.data.PostContract;
import com.yabby.flowpost.data.twitter.TwitterContract;
import com.yabby.flowpost.ui.DefaultPostViewHolder;
import com.yabby.flowpost.ui.widget.ImageSlideAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
* Created by chernykh on 10/21/2014.
*/
public class TwitterPostViewHolder extends DefaultPostViewHolder {

    public static final int LAYOUT = R.layout.post_item_twitter;

    @Bind(R.id.twitter_post_retweet) TextView mRetweetText;
    @Bind(R.id.twitter_post_favorite) TextView mFavoriteText;
    @Bind(R.id.twitter_post_author_image) ImageView mAuthorImage;
    @Bind(R.id.twitter_post_author_screen_name) TextView mAuthorScreenNameText;
    @Bind(R.id.twitter_post_media) ImageView mPostMediaImage;
    @Bind(R.id.twitter_post_retweet_info) TextView mRetweetInfoText;
    @Bind(R.id.twitter_post_media_pager) ViewPager mPostMediaPager;
    @Bind(R.id.twitter_post_media_pager_indicator) CirclePageIndicator mIndicator;
    @Bind(R.id.twitter_post_media_container) View mPostMediaContainer;

    public TwitterPostViewHolder(View view) {
        super(view);
    }

    @Override
    public void setData(Cursor cursor) {
        super.setData(cursor);

        String authorImageFile = cursor.getString(cursor.getColumnIndex(PostContract.AuthorEntry.COLUMN_IMAGE_BIG));
        Glide.with(itemView.getContext()).load(authorImageFile).into(mAuthorImage);

        String authorScreenName = cursor.getString(cursor.getColumnIndex(TwitterContract.AuthorEntry.COLUMN_SCREEN_NAME));
        mAuthorScreenNameText.setText("@" + authorScreenName);

        byte[] media = cursor.getBlob(cursor.getColumnIndex(TwitterContract.PostEntry.COLUMN_MEDIA));
        if (media != null) {
            List<String> images = new ArrayList<>();
            String[] mediaData = (String[]) DataTools.fromByteArray(media);
            if (mediaData != null) {
                for (int i = 0; i < mediaData.length / 2 ; i++) {
                    if ("photo".equals(mediaData[i * 2])) {
                        images.add(mediaData[i * 2 + 1]);
                    }
                }
            }
            if (images.size() == 1) {
                Glide.with(itemView.getContext()).load(images.get(0)).into(mPostMediaImage);
                mPostMediaImage.setVisibility(View.VISIBLE);
                mPostMediaPager.setVisibility(View.GONE);
                mIndicator.setVisibility(View.GONE);
            } else {
                mPostMediaPager.setAdapter(new ImageSlideAdapter(itemView.getContext(), images));
                mIndicator.setViewPager(mPostMediaPager);
                mIndicator.setFillColor(itemView.getResources().getColor(R.color.primary));
                mIndicator.setSnap(true);
                mPostMediaPager.setVisibility(View.VISIBLE);
                mIndicator.setVisibility(View.VISIBLE);
                mPostMediaImage.setVisibility(View.GONE);
            }
            mPostMediaContainer.setVisibility(View.VISIBLE);
        } else {
            mPostMediaContainer.setVisibility(View.GONE);
        }

        int retweetCount = cursor.getInt(cursor.getColumnIndex(TwitterContract.PostEntry.COLUMN_RETWEET_COUNT));
        int favoriteCount = cursor.getInt(cursor.getColumnIndex(TwitterContract.PostEntry.COLUMN_FAVORITE_COUNT));
        mRetweetText.setText(String.valueOf(retweetCount));
        mFavoriteText.setText(String.valueOf(favoriteCount));
        boolean favorite = cursor.getInt(cursor.getColumnIndex(TwitterContract.PostEntry.COLUMN_FAVORITE)) != 0;
        mFavoriteText.setSelected(favorite);

        long retweetedAuthor = cursor.getLong(cursor.getColumnIndex(TwitterContract.PostEntry.COLUMN_ORIGINAL_AUTHOR_ID));
        if (retweetedAuthor > 0) {
            Cursor c = itemView.getContext().getContentResolver().query(
                PostContract.AuthorEntry.buildUri(retweetedAuthor),
                null,
                null,
                null,
                null
            );
            if (c.moveToFirst()) {
                String originalAuthorName = c.getString(c.getColumnIndex(PostContract.AuthorEntry.COLUMN_NAME));
                mRetweetInfoText.setText(itemView.getContext().getString(R.string.author_retweeted, originalAuthorName));
            }
            c.close();
            mRetweetInfoText.setVisibility(View.VISIBLE);
        } else {
            mRetweetInfoText.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.twitter_post_favorite)
    public void onLikeClick() {
        mFavoriteText.setEnabled(false);
        new MarkAsFavoriteAsyncTask(TwitterContract.PostEntry.CONTENT_URI, mFavoriteText, getPostId()).execute(!mFavoriteText.isSelected());
    }
}
