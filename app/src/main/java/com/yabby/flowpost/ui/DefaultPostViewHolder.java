package com.yabby.flowpost.ui;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.yabby.flowpost.R;
import com.yabby.flowpost.data.PostContract;

/**
 * Cache of the children views for a forecast list item.
 */
public class DefaultPostViewHolder extends PostViewHolder {
    protected final TextView mDateView;
    protected final TextView mContentView;
    protected final TextView mAuthorView;

    public DefaultPostViewHolder(View view) {
        super(view);
        mDateView = (TextView) view.findViewById(R.id.post_item_date);
        mContentView = (TextView) view.findViewById(R.id.post_item_content);
        mContentView.setMovementMethod(LinkMovementMethod.getInstance());
        mAuthorView = (TextView) view.findViewById(R.id.post_item_author);
    }

    public void setData(Cursor cursor) {
        super.setData(cursor);

        String dateString = cursor.getString(cursor.getColumnIndex(PostContract.PostEntry.COLUMN_DATE));
        mDateView.setText(dateString);

        String contentString = cursor.getString(cursor.getColumnIndex(PostContract.PostEntry.COLUMN_CONTENT));
        mContentView.setText(Html.fromHtml(contentString));

        String authorString = cursor.getString(cursor.getColumnIndex(PostContract.AuthorEntry.COLUMN_NAME));
        mAuthorView.setText(authorString);
    }

    public static class MarkAsFavoriteAsyncTask extends AsyncTask<Boolean, Void, Boolean> {

        private long mPostId;

        private View mIconView;

        private Context mContext;

        private Uri mContentUri;

        public MarkAsFavoriteAsyncTask(Uri contentUri, View iconView, long postId) {
            mPostId = postId;
            mIconView = iconView;
            mContext = iconView.getContext();
            mContentUri = contentUri;
        }

        @Override
        protected Boolean doInBackground(Boolean... status) {
            ContentValues cv = new ContentValues();
            cv.put(PostContract.PostEntry.COLUMN_FAVORITE, status[0]);
            // load reshared post and show it
            int res = mContext.getContentResolver().update(mContentUri,
                    cv,
                    PostContract.PostEntry.TABLE_NAME + "." + PostContract.PostEntry._ID + "=?",
                    new String[]{String.valueOf(mPostId)});
            return res != 0;
        }

        @Override
        protected void onPostExecute(Boolean postData) {
            mIconView.setSelected(postData);
            mIconView.setEnabled(true);
        }
    }
}
