package com.yabby.flowpost.sync.twitter;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.yabby.flowpost.R;
import com.yabby.flowpost.networks.TwitterConnector;

import timber.log.Timber;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Activity to authenticate twitter
 * Created by chernykh on 10/19/2014.
 */
public class TwitterAccountAuthenticatorActivity extends AccountAuthenticatorActivity {

    public static final String TWITTER_CALLBACK_URL = "oauth://flowpost";
    private static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";

    private static Twitter twitter;
    private static User user;
    private static RequestToken requestToken;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_twitter_account_authenticator);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (twitter == null) {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(TwitterConnector.TWITTER_CONSUMER_KEY);
            builder.setOAuthConsumerSecret(TwitterConnector.TWITTER_CONSUMER_SECRET);
            Configuration configuration = builder.build();
            TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();
        }
        final Uri uri = getIntent().getData();
        if (uri != null && uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
            new AsyncTask<Void,Void,Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        // oAuth verifier
                        String verifier = uri.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
                        AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                        user = twitter.verifyCredentials();

                        String accountType = getString(R.string.sync_account_type_twitter);
                        final Account account = new Account(user.getName(), accountType);
                        // Creating the account on the device and setting the auth token we got
                        // (Not setting the auth token will cause another call to the server to authenticate the user)
                        AccountManager am = AccountManager.get(TwitterAccountAuthenticatorActivity.this);
                        Account[] twitterAccounts = am.getAccountsByType(accountType);
                        boolean accountFound = false;
                        for (Account twitterAccount : twitterAccounts) {
                            if (twitterAccount.name.equals(user.getName())) {
                                accountFound = true;
                            }
                        }
                        // if there is no such account we create it
                        if (!accountFound) {
                            am.addAccountExplicitly(account, null, null);
                        }
                        Timber.d("add account: %s %s %s", account, accessToken.getToken(), accessToken.getTokenSecret());
                        am.setAuthToken(account, "", accessToken.getToken());
                        am.setUserData(account, TwitterConnector.KEY_OAUTH_SECRET, accessToken.getTokenSecret());
                        if (!accountFound) {
                            TwitterConnector.getInstance().onAccountCreated(account, getApplicationContext());
                        }

                        // check
                        String authToken = am.peekAuthToken(account, accountType);
                        String authSecret = am.getUserData(account, TwitterConnector.KEY_OAUTH_SECRET);
                    } catch (TwitterException e) {
                        Timber.e("Unable to get auth token from url.");
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void arg) {
                    finish();
                }
            }.execute();
        } else if (twitter.getAuthorization() != null && twitter.getAuthorization().isEnabled()) {
            // already logged - no need to do anything else
            Intent res = new Intent();
            res.putExtra(AccountManager.KEY_ACCOUNT_NAME, user.getName());
            res.putExtra(AccountManager.KEY_ACCOUNT_TYPE, getString(R.string.sync_account_type_twitter));
            try {
                res.putExtra(AccountManager.KEY_AUTHTOKEN, twitter.getOAuthAccessToken().getToken());
            } catch (TwitterException e) {
                Timber.e(e, "Unable to get auth token");
            }
            setAccountAuthenticatorResult(res.getExtras());
            setResult(RESULT_OK, res);
            // clean static data
            user = null;
            twitter = null;
            requestToken = null;
            finish();
        } else {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        requestToken = twitter.getOAuthRequestToken(TWITTER_CALLBACK_URL);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL())));
                    } catch (TwitterException | IllegalStateException e) {
                        Timber.e(e, "Unable to request auth token");
                        finish();
                    }
                    return null;
                }
            }.execute();
        }
    }
}
