package com.yabby.flowpost.sync.twitter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;

import com.yabby.flowpost.R;
import com.yabby.flowpost.networks.TwitterConnector;
import com.yabby.flowpost.tools.Utils;

import javax.security.auth.login.LoginException;

import timber.log.Timber;

/**
 * SyncService for Twitter.
 * Created by chernykh on 10/4/2014.
 */
public class TwitterDataSyncService extends Service {
    private static final Object sSyncAdapterLock = new Object();
    private static AbstractThreadedSyncAdapter sDataSyncAdapter = null;

    @Override
    public void onCreate() {
        synchronized (sSyncAdapterLock) {
            if (sDataSyncAdapter == null) {
                sDataSyncAdapter = new AbstractThreadedSyncAdapter(getApplicationContext(), true) {
                    @Override
                    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
                        if (Utils.isNetworkAccessible()) {
                            Timber.d("sync start");
                            Context context = getContext();
                            AccountManager manager = AccountManager.get(context);
                            String authToken = manager.peekAuthToken(account, "");
                            String authSecret = manager.getUserData(account, TwitterConnector.KEY_OAUTH_SECRET);
                            if (!TextUtils.isEmpty(authToken) && !TextUtils.isEmpty(authSecret)) {
                                try {
                                    TwitterConnector.getInstance().loadStream(context, authToken, authSecret);
                                } catch (LoginException e) {
                                    manager.invalidateAuthToken(context.getString(R.string.sync_account_type_twitter), authToken);
                                }
                            }

                        }
                    }
                };
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sDataSyncAdapter.getSyncAdapterBinder();
    }
}
