package com.yabby.flowpost.sync.facebook;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.yabby.flowpost.sync.AbstractDataAuthenticator;

/**
 * Auth service for facebook
 * Created by chernykh on 10/22/2014.
 */
public class FacebookDataAuthenticatorService extends Service {
    // Instance field that stores the authenticator object
    private AbstractDataAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new AbstractDataAuthenticator(this, FacebookAccountAuthenticatorActivity.class);
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
