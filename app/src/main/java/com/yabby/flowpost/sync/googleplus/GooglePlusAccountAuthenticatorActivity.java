package com.yabby.flowpost.sync.googleplus;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.yabby.flowpost.R;
import com.yabby.flowpost.networks.GooglePlusConnector;
import com.yabby.flowpost.sync.AbstractDataAuthenticator;

import timber.log.Timber;

public class GooglePlusAccountAuthenticatorActivity extends AccountAuthenticatorActivity implements View.OnClickListener {
    private TextView mNameView;
    private TextView mPasswordView;
    private AccountManager mAccountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_plus_account_authenticator);
        mAccountManager = AccountManager.get(this);
        mNameView = (TextView) findViewById(R.id.username);
        Account[] accounts = mAccountManager.getAccountsByType("com.google");
        if (accounts.length > 0) {
            mNameView.setText(accounts[0].name);
        }
        mPasswordView = (TextView) findViewById(R.id.password);
        findViewById(R.id.loginButton).setOnClickListener(this);
        findViewById(R.id.cancelButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton:
                submit();
                break;
            case R.id.cancelButton:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }

    public void submit() {
        final String userName = mNameView.getText().toString();
        final String userPass = mPasswordView.getText().toString();
        new AsyncTask<Void, Void, Intent>() {
            @Override
            protected Intent doInBackground(Void... params) {
                final String type = getString(R.string.sync_account_type_googleplus);
                final Account account = new Account(userName, type);
                if (getIntent().getBooleanExtra(AbstractDataAuthenticator.ARG_IS_ADDING_NEW_ACCOUNT, false)) {
                    // Creating the account on the device and setting the auth token we got
                    // (Not setting the auth token will cause another call to the server to authenticate the user)
                    mAccountManager.addAccountExplicitly(account, userPass, null);
                }
                mAccountManager.setAuthToken(account, "", userPass);
                mAccountManager.setPassword(account, userPass);
                Timber.i("Add google+ account. Account: %s, userPass: %s, isAddingNew: %b", account, userPass,
                        getIntent().getBooleanExtra(AbstractDataAuthenticator.ARG_IS_ADDING_NEW_ACCOUNT, false));

                GooglePlusConnector.getInstance().onAccountCreated(account, getApplicationContext());

                final Intent res = new Intent();
                res.putExtra(AccountManager.KEY_ACCOUNT_NAME, userName);
                res.putExtra(AccountManager.KEY_ACCOUNT_TYPE, type);
                res.putExtra(AccountManager.KEY_AUTHTOKEN, userPass);
                setAccountAuthenticatorResult(res.getExtras());
                setResult(RESULT_OK, res);
                return res;
            }

            @Override
            protected void onPostExecute(Intent intent) {
                finish();
            }
        }.execute();
    }
}