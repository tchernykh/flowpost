package com.yabby.flowpost.sync.twitter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.yabby.flowpost.sync.AbstractDataAuthenticator;

/**
 * Auth service for twitter
 * Created by chernykh on 9/19/2014.
 */
public class TwitterDataAuthenticatorService extends Service {
    // Instance field that stores the authenticator object
    private AbstractDataAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new AbstractDataAuthenticator(this, TwitterAccountAuthenticatorActivity.class);
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
