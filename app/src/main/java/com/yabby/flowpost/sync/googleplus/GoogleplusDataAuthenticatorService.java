package com.yabby.flowpost.sync.googleplus;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.yabby.flowpost.sync.AbstractDataAuthenticator;

/**
 * DataAuthService for google+
 * Created by chernykh on 9/19/2014.
 */
public class GoogleplusDataAuthenticatorService extends Service {
    // Instance field that stores the authenticator object
    private AbstractDataAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new AbstractDataAuthenticator(this, GooglePlusAccountAuthenticatorActivity.class);
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
