package com.yabby.flowpost.sync.googleplus;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;

import com.yabby.flowpost.R;
import com.yabby.flowpost.networks.GooglePlusConnector;
import com.yabby.flowpost.tools.Utils;

import javax.security.auth.login.LoginException;

import timber.log.Timber;

/**
 * Data sync service for google+
 * Created by chernykh on 10/4/2014.
 */
public class GoogleplusDataSyncService extends Service {
    private static final Object sSyncAdapterLock = new Object();
    private static AbstractThreadedSyncAdapter sDataSyncAdapter = null;

    @Override
    public void onCreate() {
        synchronized (sSyncAdapterLock) {
            if (sDataSyncAdapter == null) {
                sDataSyncAdapter = new AbstractThreadedSyncAdapter(getApplicationContext(), true) {
                    @Override
                    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
                        if (Utils.isNetworkAccessible()) {
                            Context context = getContext();
                            Timber.i("GooglePlus sync");
                            AccountManager manager = AccountManager.get(context);
                            String pwd = manager.getPassword(account);
                            if (!TextUtils.isEmpty(pwd)) {
                                try {
                                    GooglePlusConnector.getInstance().loadStream(context, account.name, pwd);
                                } catch (LoginException e) {
                                    manager.invalidateAuthToken(context.getString(R.string.sync_account_type_googleplus), pwd);
                                }
                            }
                        }
                    }
                };
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sDataSyncAdapter.getSyncAdapterBinder();
    }
}
