package com.yabby.flowpost;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.squareup.otto.Bus;

import timber.log.Timber;

/**
 * Application instance to make all initialisation.
 * Created by chernykh on 12/14/2014.
 */
public class FlowPostApplication extends Application {

    private static Bus mBus;

    private static Context appContext;

    public static Bus bus() {
        return mBus;
    }

    // Run by PostProvider as first executed code - all initialisation that don't requires context
    // happens there
    public static void init() {
        // check whether we already initialised our app
        if (mBus == null) {
            if (BuildConfig.DEBUG) {
                Timber.plant(new Timber.DebugTree());
            } else {
                Timber.plant(new CrashReportingTree());
            }
            mBus = new Bus();
        }
    }

    static {
        init();
    }

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        if (BuildConfig.DEBUG) {
            Stetho.initialize(Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build());
        }
    }

    /** A tree which logs important information for crash reporting. */
    private static class CrashReportingTree extends Timber.HollowTree {
        @Override public void i(String message, Object... args) {
            // TODO e.g., Crashlytics.log(String.format(message, args));
        }

        @Override public void i(Throwable t, String message, Object... args) {
            i(message, args); // Just add to the log.
        }

        @Override public void e(String message, Object... args) {
            i("ERROR: " + message, args); // Just add to the log.
        }

        @Override public void e(Throwable t, String message, Object... args) {
            e(message, args);

            // TODO e.g., Crashlytics.logException(t);
        }
    }
}
