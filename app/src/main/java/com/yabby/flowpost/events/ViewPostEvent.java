package com.yabby.flowpost.events;

public class ViewPostEvent {
    private long mPostId;
    private String mNetworkName;

    public long getPostId() {
        return mPostId;
    }

    public String getNetworkName() {
        return mNetworkName;
    }

    public ViewPostEvent(String networkName, long postId) {
        mNetworkName = networkName;
        mPostId = postId;
    }
}
