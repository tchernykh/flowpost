package com.yabby.flowpost;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yabby.flowpost.data.PostContract;
import com.yabby.flowpost.ui.PostViewHolderFactory;


/**
 */
public class ViewPostFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String ARG_POST_ID = "postId";
    private static final String ARG_NETWORK_NAME = "networkName";
    private static final int POST_LOADER = 1;

    private long mPostId;
    private String mNetworkName;
    private ViewGroup mContainer;

    public static ViewPostFragment newInstance(String networkName, long postId) {
        ViewPostFragment fragment = new ViewPostFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NETWORK_NAME, networkName);
        args.putLong(ARG_POST_ID, postId);
        fragment.setArguments(args);
        return fragment;
    }

    public ViewPostFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mNetworkName = getArguments().getString(ARG_NETWORK_NAME);
            mPostId = getArguments().getLong(ARG_POST_ID);
        }
        getLoaderManager().initLoader(POST_LOADER, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_view_post, container, false);
        mContainer = (ViewGroup) root.findViewById(R.id.post_container);
        return root;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                getActivity(),
                PostContract.PostEntry.buildUri(mNetworkName, mPostId),
                // we need all columns returned by ContentProvider because what columns are
                // returned depends on networks and adapter knows how to show these data
                null,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.moveToFirst()) {
            View viewWithData = PostViewHolderFactory.createViewAndSetData(mNetworkName, getActivity(), data);
            mContainer.addView(viewWithData);
            getFlowActivity().showToolbar();
        } else {
            // TODO handle not found error
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
