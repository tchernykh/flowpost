package com.yabby.flowpost;

import android.support.v4.app.Fragment;

/**
 * Base functionality for fragments of the application.
 */
public abstract class BaseFragment extends Fragment {

    public FlowActivity getFlowActivity() {
        return (FlowActivity) super.getActivity();
    }

    public String getCustomTag() {
        return getClass().getName();
    }
}
