package com.yabby.flowpost.networks;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncRequest;
import android.os.Build;
import android.os.Bundle;

import com.yabby.flowpost.R;

/**
 * Helper class to create network connectors. Contains base methods.
 * Created by chernykh on 10/20/2014.
 */
public abstract class AbstractNetworkConnector implements NetworkConnector {

    // Interval at which to sync with the services, in seconds.
    // 60 seconds (1 minute) * 60 = 1 hour
    public static final int SYNC_INTERVAL = 60 * 60;
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;

    public abstract String getAccountType(Context context);

    public Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Look for the account
        Account[] account = accountManager.getAccountsByType(getAccountType(context));
        if (account.length > 0) {
            return account[0];
        }

        return null;
    }

    public void syncImmediately(Context context) {
        Account syncAccount = getSyncAccount(context);
        if (syncAccount != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            ContentResolver.requestSync(syncAccount, context.getString(R.string.content_authority), bundle);
        }
    }

    public void removeSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        accountManager.removeAccount(getSyncAccount(context), null, null);
    }

    /**
     * Helper method to schedule the sync adapter periodic execution
     */
    private void configurePeriodicSync(Account account, Context context, int syncInterval, int flexTime) {
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).
                    build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account, authority, new Bundle(), syncInterval);
        }
    }

    public void onAccountCreated(Account newAccount, Context context) {
        // Since we've created an account
        configurePeriodicSync(newAccount, context, SYNC_INTERVAL, SYNC_FLEXTIME);
        // Without calling setSyncAutomatically, our periodic sync will not be enabled.
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);
        // Finally, let's do a sync to get things started
        syncImmediately(context);
    }
}
