package com.yabby.flowpost.networks.facebook;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.yabby.flowpost.data.DataTools;
import com.yabby.flowpost.data.facebook.FacebookContract;
import com.yabbysoftware.socialnetwork.facebook.FacebookPost;

import java.util.Date;

/**
 * Helper to work with facebook api.
 * Created by chernykh on 12/13/2014.
 */
public class FacebookHelper {

    public static ContentValues convertToContentValues(FacebookPost post) {
        ContentValues result = new ContentValues();
//        result.put(FacebookContract.PostEntry.COLUMN_SHARE_COUNT, post.getSharedPost());
        result.put(FacebookContract.PostEntry.COLUMN_NETWORK_ID, post.getId());
        result.put(FacebookContract.PostEntry.COLUMN_CONTENT, post.getContent());
        result.put(FacebookContract.PostEntry.COLUMN_TYPE, post.getType());
//        result.put(FacebookContract.PostEntry.COLUMN_CONTENT_NAME, getName());
        result.put(FacebookContract.AuthorEntry.TABLE_NAME + "." + FacebookContract.AuthorEntry.COLUMN_NETWORK_ID, post.getAuthorId());
        result.put(FacebookContract.AuthorEntry.TABLE_NAME + "." + FacebookContract.AuthorEntry.COLUMN_NAME, post.getAuthorName());
        result.put(FacebookContract.PostEntry.COLUMN_PICTURE, post.getPhoto());
        result.put(FacebookContract.PostEntry.COLUMN_LIKES_COUNT, post.getLikesCount());
        result.put(FacebookContract.PostEntry.COLUMN_COMMENTS_COUNT, post.getCommentsCount());

        Date createdDate = new Date();
        // TODO parse date
//        try {
//            createdDate = FACEBOOK_TIME_FORMAT.parse(post.getCreatedAt());
//        } catch (ParseException e) {
//            Timber.e(e, "wrong time format from facebook");
//            createdDate = new Date();
//        }
        result.put(FacebookContract.PostEntry.COLUMN_DATE, DataTools.dateToDbString(createdDate));
        return result;
    }

    public static ContentValues convertToAuthorContentValues(FacebookPost post) {
        ContentValues result = new ContentValues();
        result.put(FacebookContract.AuthorEntry.COLUMN_NETWORK_ID, post.getAuthorId());
        result.put(FacebookContract.AuthorEntry.COLUMN_NAME, post.getAuthorName());
        result.put(FacebookContract.AuthorEntry.COLUMN_IMAGE_BIG, getAuthorPictureUrl(post.getAuthorId()));
        return result;
    }

    public static void loadAuthorImages(Context context, FacebookPost post) {
        // TODO can actually request any size by sending width and height params
        Glide.with(context).load(getAuthorPictureUrl(post.getAuthorId())).downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
    }

    public static String getAuthorPictureUrl(String authorNetworkId) {
        return "https://graph.facebook.com/" + authorNetworkId + "/picture?type=large";
    }

    public static void loadPostImages(Context context, FacebookPost post) {
        String picture = post.getPhoto();
        if (picture != null) {
            Uri pictureUri = Uri.parse(picture);
            String src = pictureUri.getQueryParameter("src");
            if (src == null) {
                src = pictureUri.getQueryParameter("url");
            }
            if (src == null) {
                src = picture;
            }
            Glide.with(context).load(src).downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            post.setPhoto(src);
        }
    }
}
