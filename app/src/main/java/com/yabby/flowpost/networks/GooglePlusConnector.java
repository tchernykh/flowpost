package com.yabby.flowpost.networks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.yabby.flowpost.R;
import com.yabby.flowpost.data.DataTools;
import com.yabby.flowpost.data.googleplus.GooglePlusContract;
import com.yabby.flowpost.tools.Utils;
import com.yabby.flowpost.ui.UiUtils;
import com.yabbysoftware.network.SerializableCookie;
import com.yabbysoftware.socialnetwork.googleplus.GPlusAuthor;
import com.yabbysoftware.socialnetwork.googleplus.GPlusCredentials;
import com.yabbysoftware.socialnetwork.googleplus.GPlusNetwork;
import com.yabbysoftware.socialnetwork.googleplus.GPlusPost;
import com.yabbysoftware.socialnetwork.googleplus.GPlusUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;

import timber.log.Timber;

/**
 * Connector for Google+ network
 * Created by chernykh on 9/20/2014.
 */
public class GooglePlusConnector extends AbstractNetworkConnector {

    private static GooglePlusConnector INSTANCE;

    private static final String COOKIES_STORAGE_FILE = "gplusCookies.ser";

    public static GooglePlusConnector getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new GooglePlusConnector();
        }
        return INSTANCE;
    }

    private GooglePlusConnector() {
    }

    public void loadStream(Context context, String username, String password) throws LoginException {
        Timber.d("loadStream: %s, %s", username, password);
        try {
            final GPlusNetwork gplus = new GPlusNetwork(NetworkFactory.getHttpClient(), NetworkFactory.getCookieManager());
            loginIfNeeded(gplus, context, username, password);
            List<GPlusPost> gPlusPosts = gplus.loadLatestPosts();
            Timber.d("Loaded %d posts", gPlusPosts.size());
            saveLoadedPosts(context, gplus, gPlusPosts);
        } catch (Exception e) {
            Timber.e(e, "unable to load posts from google plus");
        }
    }

    @SuppressWarnings("unchecked")
    private void loginIfNeeded(GPlusNetwork gPlusNetwork, Context context, String username, String password) throws LoginException {
        File cookiesStorageFile = context.getFileStreamPath(COOKIES_STORAGE_FILE);
        if (cookiesStorageFile.exists()) {
            ObjectInputStream input = null;
            try {
                FileInputStream f = new FileInputStream(cookiesStorageFile);
                InputStream buffer = new BufferedInputStream(f);
                input = new ObjectInputStream(buffer);
                List<SerializableCookie> loadedCookies = (List<SerializableCookie>) input.readObject();
                if (loadedCookies == null || loadedCookies.isEmpty()) {
                    Timber.e("Unable to load cookies from %s - cookie list is empty", cookiesStorageFile.getPath());
                } else {
                    gPlusNetwork.login(SerializableCookie.fromSerializable(loadedCookies));
                    Timber.d("Login cookies loaded from file");
                    return;
                }
            } catch (Exception ex) {
                Timber.e(ex, "Unable to load cookies from %s - removing the file", cookiesStorageFile.getPath());
                if (cookiesStorageFile.delete()) {
                    Timber.d("File %s was successfully removed", cookiesStorageFile.getAbsolutePath());
                }
            } finally {
                Utils.safeClose(input);
            }
        }

        GPlusCredentials gPlusCredentials = new GPlusCredentials();
        gPlusCredentials.setPassword(password);
        gPlusCredentials.setUsername(username);
        OutputStream fileOutputStream = null;
        try {
            List<HttpCookie> cookies = gPlusNetwork.login(gPlusCredentials);
            if (!cookiesStorageFile.createNewFile()) {
                throw new IllegalStateException("Unable to create file " + cookiesStorageFile.getPath());
            }
            fileOutputStream = new FileOutputStream(cookiesStorageFile);
            ObjectOutputStream stream = new ObjectOutputStream(new BufferedOutputStream(fileOutputStream));
            stream.writeObject(SerializableCookie.toSerializable(cookies));
            stream.close();
            Timber.d("Login cookies stored to file");
        } catch (Exception e) {
            Timber.e(e, "Unable to login to googleplus using username and password provided by user");
            Timber.d("Probably wrong credentials: uname: %s pass: %s", username, password);
        } finally {
            Utils.safeClose(fileOutputStream);
        }
    }

    private void saveLoadedPosts(Context context, GPlusNetwork gPlusNetwork, List<GPlusPost> statuses) throws Exception {
        Map<String, ContentValues> authorValuesToSave = new HashMap<>();
        List<ContentValues> postsValuesToSave = new ArrayList<>();

        for (GPlusPost post : statuses) {
            putAuthorValuesIfNeeded(authorValuesToSave, post);

            postsValuesToSave.add(postToContentValues(post));
        }

        final Collection<ContentValues> authors = authorValuesToSave.values();
        loadAuthorsData(context, gPlusNetwork, authors);

        Map<String, Integer> authorsMap = new HashMap<>();
        final Iterator<ContentValues> iter = authors.iterator();
        while (iter.hasNext()) {
            ContentValues author = iter.next();
            Integer authorId = author.getAsInteger(GooglePlusContract.AuthorEntry._ID);
            if (authorId != null) {
                String authorGPlusId = author.getAsString(GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID);
                authorsMap.put(authorGPlusId, authorId);
                iter.remove();
            }
        }

        ContentValues[] authorValues = authors.toArray(new ContentValues[authors.size()]);
        context.getContentResolver().bulkInsert(GooglePlusContract.AuthorEntry.CONTENT_URI, authorValues);
        for (ContentValues authorValue : authorValues) {
            Integer authorId = authorValue.getAsInteger(GooglePlusContract.AuthorEntry._ID);
            String authorGPlusId = authorValue.getAsString(GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID);
            authorsMap.put(authorGPlusId, authorId);
        }

        // authorValues should be updated with _ID information that we must use in gplusPostValues instead of author id
        ContentValues[] postsValues = postsValuesToSave.toArray(new ContentValues[postsValuesToSave.size()]);
        for (ContentValues postsValue : postsValues) {
            final String authorId = postsValue.getAsString(GooglePlusContract.PostEntry.COLUMN_AUTHOR_ID);
            if (authorId != null) {
                postsValue.put(GooglePlusContract.PostEntry.COLUMN_AUTHOR_ID, authorsMap.get(authorId));
            }
            String picture = postsValue.getAsString(GooglePlusContract.PostEntry.COLUMN_CONTENT_PICTURE);
            if (picture != null) {
                picture = GPlusUtils.getImageUriWithWidth(picture, UiUtils.getMaxScreenSize());
                Glide.with(context).load(Utils.normalizeUrl(picture)).downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
                postsValue.put(GooglePlusContract.PostEntry.COLUMN_CONTENT_PICTURE, picture);
            }
        }
        int inserted = context.getContentResolver().bulkInsert(GooglePlusContract.PostEntry.CONTENT_URI, postsValues);
        Timber.d("GPlus data load successfully: %d new posts loaded", inserted);
    }

    private void putAuthorValuesIfNeeded(Map<String, ContentValues> authorValuesToSave, GPlusPost post) {
        ContentValues uv = authorValuesToSave.get(post.getAuthorId());
        if (uv == null) {
            uv = new ContentValues();
            uv.put(GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID, post.getAuthorId());
            uv.put(GooglePlusContract.AuthorEntry.COLUMN_NAME, post.getAuthorName());
            authorValuesToSave.put(post.getAuthorId(), uv);
        }
    }

    private ContentValues postToContentValues(GPlusPost post) {
        ContentValues pv = new ContentValues();
        pv.put(GooglePlusContract.PostEntry.COLUMN_AUTHOR_ID, post.getAuthorId());
        pv.put(GooglePlusContract.PostEntry.COLUMN_DATE, gPlusDateToDbString(post.getCreatedAt()));
        pv.put(GooglePlusContract.PostEntry.COLUMN_CONTENT, post.getContent() == null? "" : post.getContent());
        pv.put(GooglePlusContract.PostEntry.COLUMN_CATEGORY, post.getCategory());
        pv.put(GooglePlusContract.PostEntry.COLUMN_TAG, post.getTag());
        pv.put(GooglePlusContract.PostEntry.COLUMN_NETWORK_ID, post.getId());

        pv.put(GooglePlusContract.PostEntry.COLUMN_CONTENT_PICTURE, post.getMediaContentImage());
        pv.put(GooglePlusContract.PostEntry.COLUMN_CONTENT_YOUTUBE, post.getMediaContentYoutube());
        pv.put(GooglePlusContract.PostEntry.COLUMN_CONTENT_LINK, post.getMediaContentLink());
        pv.put(GooglePlusContract.PostEntry.COLUMN_CONTENT_DESCRIPTION, post.getMediaContentDescription());
        pv.put(GooglePlusContract.PostEntry.COLUMN_SHARE_COUNT, post.getSharesCount());
        pv.put(GooglePlusContract.PostEntry.COLUMN_LIKES_COUNT, post.getLikesCount());
        pv.put(GooglePlusContract.PostEntry.COLUMN_COMMENTS_COUNT, post.getCommentsCount());

        pv.put(GooglePlusContract.PostEntry.COLUMN_COMMENT_AUTHOR, post.getTopCommentAuthor());
        pv.put(GooglePlusContract.PostEntry.COLUMN_COMMENT_DESCRIPTION, post.getTopCommentDescription());

        pv.put(GooglePlusContract.PostEntry.COLUMN_RESHARED_POST_AUTHOR_ID, post.getResharedAuthorId());
        pv.put(GooglePlusContract.PostEntry.COLUMN_POST_RESHARE_CONTENT, post.getReshareContent());

        // TODO save comments
        return pv;
    }

    @NonNull
    private String gPlusDateToDbString(String date) {
        if (date != null) {
            try {
                return DataTools.dateToDbString(GPlusUtils.toDate(date, new Date()));
            } catch (Exception ex) {
                Timber.e(ex, "Incorrect createdAt string from server %s", date);
            }
        }
        return "";
    }

    private void loadAuthorsData(Context context, GPlusNetwork loader, Collection<ContentValues> authors) throws Exception {
        if (authors.isEmpty()) return;
        String[] authorsId = new String[authors.size()];
        int i = 0;
        for (ContentValues author : authors) {
            authorsId[i++] = author.getAsString(GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID);
        }
        Cursor c = context.getContentResolver().query(GooglePlusContract.AuthorEntry.CONTENT_URI,
                new String[]{GooglePlusContract.AuthorEntry._ID, GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID},
                GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID + " IN (" + makePlaceholders(authors.size()) + ")",
                authorsId,
                null);
        try {
            int idInd = c.getColumnIndex(GooglePlusContract.AuthorEntry._ID);
            int networkIdInd = c.getColumnIndex(GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID);
            if (c.moveToFirst()) {
                String id =  c.getString(idInd);
                String networkId =  c.getString(networkIdInd);
                for (ContentValues author : authors) {
                    if (networkId.equals(author.getAsString(GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID))) {
                        // we already have it - TODO check last update time
                        author.put(GooglePlusContract.AuthorEntry._ID, id);
                        break;
                    }
                }
            }
            for (ContentValues author : authors) {
                if (!author.containsKey(GooglePlusContract.AuthorEntry._ID)) {
                    String authorId = author.getAsString(GooglePlusContract.AuthorEntry.COLUMN_NETWORK_ID);
                    GPlusAuthor authorData = loader.loadAuthor(authorId);
                    if (authorData != null) {
                        String authorPhoto = Utils.normalizeUrl(authorData.getAuthorPhoto());
                        Timber.d("Loading author's photo: %s", authorPhoto);
                        Glide.with(context).load(authorPhoto).downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
                        author.put(GooglePlusContract.AuthorEntry.COLUMN_IMAGE_BIG, authorPhoto);
                    } else {
                        Timber.w("Author id: %s not found to load pics", authorId);
                    }
                }
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    private String makePlaceholders(int len) {
        StringBuilder sb = new StringBuilder(len * 2 - 1);
        sb.append("?");
        for (int i = 1; i < len; i++) {
            sb.append(",?");
        }
        return sb.toString();
    }

    @Override
    public String getAccountType(Context context) {
        return context.getString(R.string.sync_account_type_googleplus);
    }
}
