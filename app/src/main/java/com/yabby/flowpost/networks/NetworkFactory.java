package com.yabby.flowpost.networks;

import com.squareup.okhttp.OkHttpClient;

import java.net.CookieManager;

/**
 * Provides instances for network
 */
public class NetworkFactory {

    private static OkHttpClient mHttpClient;
    private static CookieManager mCookieHandler;

    public static OkHttpClient getHttpClient() {
        if (mHttpClient == null) {
            mHttpClient = new OkHttpClient();
        }
        return mHttpClient;
    }

    public static CookieManager getCookieManager() {
        if (mCookieHandler == null) {
            mCookieHandler = new CookieManager();
        }
        return mCookieHandler;
    }
}
