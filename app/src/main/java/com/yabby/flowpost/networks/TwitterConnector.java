package com.yabby.flowpost.networks;

import android.accounts.AccountManager;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.yabby.flowpost.R;
import com.yabby.flowpost.data.DataTools;
import com.yabby.flowpost.data.twitter.TwitterContract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;

import timber.log.Timber;
import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Connector for Twitter network
 * Created by chernykh on 9/20/2014.
 */
public class TwitterConnector extends AbstractNetworkConnector {

    public static final String TWITTER_CONSUMER_KEY = "eUEuzFnxQ3IMF5UZFTHkAM6It";
    public static final String TWITTER_CONSUMER_SECRET = "iMYuxDWcfuRgW6aFTHfqltxo2D84MnxV2uwdwMbjGlSdtxy8hD";
    public static final String KEY_OAUTH_SECRET = "oauth_token_secret";

    private static TwitterConnector INSTANCE;

    public static TwitterConnector getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TwitterConnector();
        }
        return INSTANCE;
    }

    private TwitterConnector() {
    }

    public void loadStream(final Context context, final String authToken, String authSecret) throws LoginException {
        Timber.d("loadStream from twitter: secret - %s token - %s", authSecret, authToken);
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
        builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
        builder.setOAuthAccessToken(authToken);
        builder.setOAuthAccessTokenSecret(authSecret);
        Configuration configuration = builder.build();
        TwitterFactory factory = new TwitterFactory(configuration);
        final Twitter twitter = factory.getInstance();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    twitter.verifyCredentials();
                    List<twitter4j.Status> statuses = twitter.getHomeTimeline();
                    Timber.d("loadStream loaded %d statuses", statuses.size());
                    saveLoadedStatuses(context, statuses);
                } catch (TwitterException e) {
                    if (e.getStatusCode() == 401) {
                        Timber.w("twitter auth token invalidated");
                        AccountManager manager = AccountManager.get(context);
                        manager.invalidateAuthToken(getAccountType(context), authToken);
                    }
                }
                return null;
            }
        }.execute();
    }

    private void saveLoadedStatuses(Context context, List<Status> statuses) {
        Map<Long, ContentValues> authorValuesToSave = new HashMap<>();
        List<ContentValues> postsValuesToSave = new ArrayList<>();

        for (Status status : statuses) {
            User u = status.getUser();
            saveUserData(context, authorValuesToSave, u);

            ContentValues pv = new ContentValues();
            pv.put(TwitterContract.PostEntry.COLUMN_AUTHOR_ID, u.getId());
            Status retweetedStatus = status.getRetweetedStatus();
            long originalAuthor = -1;
            if (retweetedStatus != null) {
                User originalUser = retweetedStatus.getUser();
                saveUserData(context, authorValuesToSave, originalUser);
                status = retweetedStatus;
                originalAuthor = originalUser.getId();
            }
            pv.put(TwitterContract.PostEntry.COLUMN_ORIGINAL_AUTHOR_ID, originalAuthor);
            pv.put(TwitterContract.PostEntry.COLUMN_DATE, DataTools.dateToDbString(status.getCreatedAt()));
            String statusText = getStatusContent(status);
            pv.put(TwitterContract.PostEntry.COLUMN_CONTENT, statusText);
            pv.put(TwitterContract.PostEntry.COLUMN_NETWORK_ID, status.getId());
            MediaEntity[] mediaEntities = status.getExtendedMediaEntities();
            if (mediaEntities != null && mediaEntities.length > 0) {
                String[] mediaToStore = new String[mediaEntities.length * 2];
                int ind = 0;
                for (MediaEntity mediaEntity : mediaEntities) {
                    mediaToStore[ind++] = mediaEntity.getType();
                    String imagePath = mediaEntity.getMediaURL();
                    Glide.with(context).load(imagePath).downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
                    mediaToStore[ind++] = imagePath;
                }
                byte[] data = DataTools.toByteArray(mediaToStore);
                if (data != null) {
                    pv.put(TwitterContract.PostEntry.COLUMN_MEDIA, data);
                }
            }
            postsValuesToSave.add(pv);
        }

        final Collection<ContentValues> authors = authorValuesToSave.values();
        ContentValues[] authorValues = authors.toArray(new ContentValues[authors.size()]);
        context.getContentResolver().bulkInsert(TwitterContract.AuthorEntry.CONTENT_URI, authorValues);
        Map<String, Integer> twitterAuthorsMap = new HashMap<>();
        for (ContentValues authorValue : authorValues) {
            Integer authorId = authorValue.getAsInteger(TwitterContract.AuthorEntry._ID);
            String authorName = authorValue.getAsString(TwitterContract.AuthorEntry.COLUMN_NETWORK_ID);
            twitterAuthorsMap.put(authorName, authorId);
        }

        // authorValues should be updated with _ID information that we must use in facebookPostValues instead of author facebook id
        ContentValues[] postsValues = postsValuesToSave.toArray(new ContentValues[postsValuesToSave.size()]);
        for (ContentValues postsValue : postsValues) {
            Integer originalAuthorId = twitterAuthorsMap.get(postsValue.getAsString(TwitterContract.PostEntry.COLUMN_ORIGINAL_AUTHOR_ID));
            if (originalAuthorId != null && originalAuthorId != -1) {
                Integer authorId = twitterAuthorsMap.get(postsValue.getAsString(TwitterContract.PostEntry.COLUMN_AUTHOR_ID));
                Integer oAuthorId = twitterAuthorsMap.get(postsValue.getAsString(TwitterContract.PostEntry.COLUMN_ORIGINAL_AUTHOR_ID));
                postsValue.put(TwitterContract.PostEntry.COLUMN_ORIGINAL_AUTHOR_ID, authorId);
                postsValue.put(TwitterContract.PostEntry.COLUMN_AUTHOR_ID, oAuthorId);
            } else {
                Integer authorId = twitterAuthorsMap.get(postsValue.getAsString(TwitterContract.PostEntry.COLUMN_AUTHOR_ID));
                postsValue.put(TwitterContract.PostEntry.COLUMN_AUTHOR_ID, authorId);
                postsValue.remove(TwitterContract.PostEntry.COLUMN_ORIGINAL_AUTHOR_ID);
            }
        }
        int inserted = context.getContentResolver().bulkInsert(TwitterContract.PostEntry.CONTENT_URI, postsValues);
        Timber.d("Twitter data load successfully: %d new posts loaded", inserted);
    }

    private String getStatusContent(Status status) {
        String result = status.getText();
        // process all urls
        if (status.getURLEntities() != null) {
            for (URLEntity urlEntity : status.getURLEntities()) {
                result = result.replace(urlEntity.getURL(),
                    "<a href='" + urlEntity.getExpandedURL() + "'>" + urlEntity.getDisplayURL() + "</a>");
            }
        }
        return result;
    }

    private void saveUserData(Context context, Map<Long, ContentValues> authorValuesToSave, User u) {
        ContentValues uv = authorValuesToSave.get(u.getId());
        if (uv == null) {
            uv = new ContentValues();
            uv.put(TwitterContract.AuthorEntry.COLUMN_NETWORK_ID, u.getId());
            uv.put(TwitterContract.AuthorEntry.COLUMN_NAME, u.getName());
            uv.put(TwitterContract.AuthorEntry.COLUMN_SCREEN_NAME, u.getScreenName());
            Glide.with(context).load(u.getMiniProfileImageURL()).downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            Glide.with(context).load(u.getBiggerProfileImageURL()).downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            uv.put(TwitterContract.AuthorEntry.COLUMN_IMAGE_SMALL, u.getMiniProfileImageURL());
            uv.put(TwitterContract.AuthorEntry.COLUMN_IMAGE_BIG, u.getBiggerProfileImageURL());

            // name that writes after @
            String screenName = u.getScreenName();
            String miniImageFileName =  screenName + "_mini.jpg";
            String biggerImageFileName = screenName + "_big.jpg";

            // TODO use all data
//                String bannerImageFileName = u.getMiniProfileImageURL().substring(u.getProfileBannerURL().lastIndexOf("/") + 1);
//                String userDescription = u.getDescription();
//                Date userCreatedAt = u.getCreatedAt();
            authorValuesToSave.put(u.getId(), uv);
        }
    }

    @Override
    public String getAccountType(Context context) {
        return context.getString(R.string.sync_account_type_twitter);
    }
}
