package com.yabby.flowpost.networks;

import android.content.ContentValues;
import android.content.Context;

import com.yabby.flowpost.R;
import com.yabby.flowpost.data.facebook.FacebookContract;
import com.yabby.flowpost.networks.facebook.FacebookHelper;
import com.yabby.flowpost.tools.Utils;
import com.yabbysoftware.network.SerializableCookie;
import com.yabbysoftware.socialnetwork.facebook.FacebookCredentials;
import com.yabbysoftware.socialnetwork.facebook.FacebookNetwork;
import com.yabbysoftware.socialnetwork.facebook.FacebookPost;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;

import timber.log.Timber;

/**
 * Connector for Facebook network
 * Created by chernykh on 9/20/2014.
 */
public class FacebookConnector extends AbstractNetworkConnector {

    private static FacebookConnector INSTANCE;

    private static final String COOKIES_STORAGE_FILE = "facebookCookies.ser";

    public static FacebookConnector getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FacebookConnector();
        }
        return INSTANCE;
    }

    private FacebookConnector() {
    }

    public void loadStream(Context context, String username, String password) throws LoginException {
        try {
            FacebookNetwork facebookNetwork = new FacebookNetwork(NetworkFactory.getHttpClient(), NetworkFactory.getCookieManager());
            loginIfNeeded(facebookNetwork, context, username, password);
            List<FacebookPost> posts = facebookNetwork.loadLatestPosts();
            Timber.d("Loaded %d posts", posts.size());
            saveLoadedPosts(context, posts);
        } catch (Exception e) {
            Timber.e(e, "unable to load posts from facebook");
        }
    }

    @SuppressWarnings("unchecked")
    private void loginIfNeeded(FacebookNetwork facebookNetwork, Context context, String username, String password) {
        File cookiesStorageFile = context.getFileStreamPath(COOKIES_STORAGE_FILE);
        if (cookiesStorageFile.exists()) {
            ObjectInputStream input = null;
            try {
                FileInputStream f = new FileInputStream(cookiesStorageFile);
                InputStream buffer = new BufferedInputStream(f);
                input = new ObjectInputStream(buffer);
                List<SerializableCookie> loadedCookies = (List<SerializableCookie>) input.readObject();
                if (loadedCookies == null || loadedCookies.isEmpty()) {
                    Timber.e("Unable to load facebook cookies from %s - cookie list is empty", cookiesStorageFile.getPath());
                } else {
                    facebookNetwork.login(SerializableCookie.fromSerializable(loadedCookies));
                    Timber.d("Facebook login cookies loaded from file");
                    return;
                }
            } catch (Exception ex) {
                Timber.e(ex, "Unable to load facebook cookies from %s", cookiesStorageFile.getPath());
            } finally {
                Utils.safeClose(input);
            }
        }

        FacebookCredentials facebookCredentials = new FacebookCredentials();
        facebookCredentials.setPassword(password);
        facebookCredentials.setUsername(username);
        try {
            List<HttpCookie> cookies = facebookNetwork.login(facebookCredentials);
            ObjectOutputStream output = null;
            try {
                if (!cookiesStorageFile.createNewFile()) {
                    throw new IllegalStateException("Unable to create file " + cookiesStorageFile.getPath());
                }
                OutputStream f = new FileOutputStream(cookiesStorageFile);
                OutputStream buffer = new BufferedOutputStream(f);
                output = new ObjectOutputStream(buffer);
                output.writeObject(SerializableCookie.toSerializable(cookies));
                Timber.d("Facebook login cookies stored to file");
            } finally {
                Utils.safeClose(output);
            }
        } catch (Exception e) {
            Timber.e(e, "Unable to login to facebook using username and password provided by user");
        }
    }

    private void saveLoadedPosts(Context context, List<FacebookPost> posts) {
        Map<String, ContentValues> authorValuesToSave = new HashMap<>();
        List<ContentValues> postsValuesToSave = new ArrayList<>();
        for (FacebookPost post : posts) {
            FacebookHelper.loadPostImages(context, post);
            ContentValues facebookPostValues = FacebookHelper.convertToContentValues(post);
            postsValuesToSave.add(facebookPostValues);

            String authorFacebookId = post.getAuthorId();
            if (authorValuesToSave.get(authorFacebookId) == null) {
                FacebookHelper.loadAuthorImages(context, post);
                authorValuesToSave.put(authorFacebookId, FacebookHelper.convertToAuthorContentValues(post));
            }
        }

        final Collection<ContentValues> authors = authorValuesToSave.values();
        ContentValues[] authorValues = authors.toArray(new ContentValues[authors.size()]);
        context.getContentResolver().bulkInsert(FacebookContract.AuthorEntry.CONTENT_URI, authorValues);
        Map<String, Integer> facebookAuthorsMap = new HashMap<>();
        for (ContentValues authorValue : authorValues) {
            Integer authorId = authorValue.getAsInteger(FacebookContract.AuthorEntry._ID);
            String authorName = authorValue.getAsString(FacebookContract.AuthorEntry.COLUMN_NETWORK_ID);
            facebookAuthorsMap.put(authorName, authorId);
        }

        // authorValues should be updated with _ID information that we must use in facebookPostValues instead of author facebook id
        ContentValues[] postsValues = postsValuesToSave.toArray(new ContentValues[postsValuesToSave.size()]);
        for (ContentValues postsValue : postsValues) {
            Integer authorId = facebookAuthorsMap.get(postsValue.getAsString(FacebookContract.AuthorEntry.TABLE_NAME + "." + FacebookContract.AuthorEntry.COLUMN_NETWORK_ID));
            postsValue.remove(FacebookContract.AuthorEntry.TABLE_NAME + "." + FacebookContract.AuthorEntry.COLUMN_NETWORK_ID);
            postsValue.remove(FacebookContract.AuthorEntry.TABLE_NAME + "." + FacebookContract.AuthorEntry.COLUMN_NAME);
            postsValue.put(FacebookContract.PostEntry.COLUMN_AUTHOR_ID, authorId);
        }
        int inserted = context.getContentResolver().bulkInsert(FacebookContract.PostEntry.CONTENT_URI, postsValues);
        Timber.i("Facebook data load successfully: %d new posts loaded", inserted);
    }

    @Override
    public String getAccountType(Context context) {
        return context.getString(R.string.sync_account_type_facebook);
    }
}
