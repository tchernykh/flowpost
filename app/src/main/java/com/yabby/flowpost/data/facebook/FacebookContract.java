package com.yabby.flowpost.data.facebook;

import android.net.Uri;

import com.yabby.flowpost.data.PostContract;

/**
 * Contract for facebook data
 */
public class FacebookContract extends PostContract{

    public static final String FACEBOOK = "facebook";

    @Override
    public PostContract.PostEntry getPostEntry() {
        return new PostEntry();
    }

    @Override
    public PostContract.AuthorEntry getAuthorEntry() {
        return new AuthorEntry();
    }

    @Override
    public String getName() {
        return FACEBOOK;
    }

    public static final class PostEntry extends PostContract.PostEntry {
        public static final Uri CONTENT_URI = PostContract.PostEntry.CONTENT_URI.buildUpon().appendPath(FACEBOOK).build();

        public static final String COLUMN_TYPE = "type";

        public static final String COLUMN_PICTURE = "picture";

        public static final String COLUMN_LIKES_COUNT = "likes_count";

        public static final String COLUMN_COMMENTS_COUNT = "comments_count";

        public static final String COLUMN_SHARE_COUNT = "share_count";

        public String getInsertSpecificColumnsQuery() {
            return COLUMN_TYPE + " TEXT, " +
                   COLUMN_PICTURE + " TEXT, " +
                   COLUMN_LIKES_COUNT + " INTEGER," +
                   COLUMN_COMMENTS_COUNT + " INTEGER," +
                   COLUMN_SHARE_COUNT + " INTEGER";
        }
    }

    public static final class AuthorEntry extends PostContract.AuthorEntry {
        public static final Uri CONTENT_URI = PostContract.AuthorEntry.CONTENT_URI.buildUpon().appendPath(FACEBOOK).build();

        public String getInsertSpecificColumnsQuery() {
            // no specific columns yet
            return "";
        }
    }
}
