package com.yabby.flowpost.data.googleplus;

import android.net.Uri;

import com.yabby.flowpost.data.PostContract;

/**
 * Contract for facebook data
 */
public class GooglePlusContract extends PostContract {

    public static final String GOOGLEPLUS = "googleplus";

    @Override
    public PostContract.PostEntry getPostEntry() {
        return new PostEntry();
    }

    @Override
    public PostContract.AuthorEntry getAuthorEntry() {
        return new AuthorEntry();
    }

    @Override
    public String getName() {
        return GOOGLEPLUS;
    }

    /**
     * GPlusPost.id -> COLUMN_NETWORK_ID
     * GPlusPost.content -> COLUMN_CONTENT
     * GPlusPost.createdAt -> COLUMN_DATE
     * GPlusPost.authorName ->
     * GPlusPost.authorId -> COLUMN_AUTHOR_ID
     * GPlusPost.category -> COLUMN_CATEGORY
     * GPlusPost.tag -> COLUMN_TAG
     * GPlusPost.mediaContentYoutube -> COLUMN_CONTENT_YOUTUBE
     * GPlusPost.mediaContentImage -> COLUMN_CONTENT_PICTURE
     * GPlusPost.mediaContentMore ->
     * GPlusPost.mediaContentMoreLink ->
     * GPlusPost.mediaContentLink -> COLUMN_CONTENT_LINK
     * GPlusPost.mediaContentDescription -> COLUMN_CONTENT_DESCRIPTION
     * GPlusPost.likesCount -> COLUMN_LIKES_COUNT
     * GPlusPost.sharesCount -> COLUMN_SHARE_COUNT
     * GPlusPost.commentsCount -> COLUMN_COMMENTS_COUNT
     * GPlusPost.topCommentAuthor -> COLUMN_COMMENT_AUTHOR
     * GPlusPost.topCommentDescription -> COLUMN_COMMENT_DESCRIPTION
     * GPlusPost.resharedAuthorName ->
     * GPlusPost.resharedAuthorId -> COLUMN_RESHARED_POST_AUTHOR_ID
     * GPlusPost.reshareContent ->
     */
    public static final class PostEntry extends PostContract.PostEntry {
        public static final Uri CONTENT_URI = PostContract.PostEntry.CONTENT_URI.buildUpon().appendPath(GOOGLEPLUS).build();

        public static final String COLUMN_CATEGORY = "category";

        public static final String COLUMN_TAG = "tag";

        public static final String COLUMN_CONTENT_PICTURE = "content_picture";

        public static final String COLUMN_CONTENT_YOUTUBE = "content_youtube";

        public static final String COLUMN_CONTENT_LINK = "content_link";

        public static final String COLUMN_CONTENT_DESCRIPTION = "content_description";

        public static final String COLUMN_LIKES_COUNT = "likes_count";

        public static final String COLUMN_COMMENTS_COUNT = "comments_count";

        public static final String COLUMN_SHARE_COUNT = "share_count";

        public static final String COLUMN_COMMENT_AUTHOR = "comment_author";

        public static final String COLUMN_COMMENT_DESCRIPTION = "comment_description";

        // author id of reshared post
        public static final String COLUMN_RESHARED_POST_AUTHOR_ID = "reshared_post_author_id";

        // comment content made to reshared post
        public static final String COLUMN_POST_RESHARE_CONTENT = "post_reshare_content";

        public String getInsertSpecificColumnsQuery() {
            return COLUMN_CATEGORY + " TEXT, " +
                    COLUMN_TAG + " TEXT, " +
                    COLUMN_CONTENT_PICTURE + " TEXT, " +
                    COLUMN_CONTENT_YOUTUBE + " TEXT, " +
                    COLUMN_CONTENT_LINK + " TEXT, " +
                    COLUMN_CONTENT_DESCRIPTION + " TEXT, " +
                    COLUMN_COMMENT_AUTHOR + " TEXT, " +
                    COLUMN_COMMENT_DESCRIPTION + " TEXT, " +
                    COLUMN_LIKES_COUNT + " INTEGER," +
                    COLUMN_COMMENTS_COUNT + " INTEGER," +
                    COLUMN_SHARE_COUNT + " INTEGER," +
                    COLUMN_RESHARED_POST_AUTHOR_ID + " TEXT, " +
                    COLUMN_POST_RESHARE_CONTENT + " TEXT";
        }
    }

    public static final class AuthorEntry extends PostContract.AuthorEntry {
        public static final Uri CONTENT_URI = PostContract.AuthorEntry.CONTENT_URI.buildUpon().appendPath(GOOGLEPLUS).build();

        public String getInsertSpecificColumnsQuery() {
            // no specific columns yet
            return "";
        }
    }
}
