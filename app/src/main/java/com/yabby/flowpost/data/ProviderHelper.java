package com.yabby.flowpost.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

/**
 * Provide Helper interface that should be implemented by all network specific services to be
 * able to easily integrate to PostProvider
 * Created by chernykh on 10/21/2014.
 */
public interface ProviderHelper {
    String DEFAULT_POST_ORDER = PostContract.PostEntry.COLUMN_DATE + " DESC";

    Cursor getPostCursor(String[] projection, String selection, String[] selectionArgs, String sortOrder);

    String getName();

    Cursor getAuthorCursor(String[] projection, String selection, String[] selectionArgs, String sortOrder);

    Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder);

    String getType(Uri uri);

    Uri insert(Uri uri, ContentValues contentValues);

    int bulkInsert(Uri uri, ContentValues[] values);

    int delete(Uri uri, String projection, String[] projectionArgs);

    int update(Uri uri, ContentValues contentValues, String projection, String[] projectionArgs);

    boolean isMatch(Uri uri);
}
