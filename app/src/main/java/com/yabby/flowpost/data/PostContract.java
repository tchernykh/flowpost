package com.yabby.flowpost.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract for common data
 */
public abstract class PostContract {

    public static final String CONTENT_AUTHORITY = "com.yabby.flowpost";

    public static final String PATH_POST = "post";

    public static final String PATH_AUTHOR = "author";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + PostContract.CONTENT_AUTHORITY + "/");

    public String getDefaultFlowSelection() {
        return null;
    }

    public static abstract class PostEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_POST).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + PostContract.CONTENT_AUTHORITY + "/" + PATH_POST;

        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + PostContract.CONTENT_AUTHORITY + "/" + PATH_POST;

        public static final String TABLE_NAME = "post";

        // id of post in social network
        public static final String COLUMN_NETWORK_ID = "network_id";

        // text content of a post
        public static final String COLUMN_CONTENT = "content";

        // date when this post were posted
        public static final String COLUMN_DATE = "post_date";

        // author of the post - probably dependent on source
        public static final String COLUMN_AUTHOR_ID = "author";

        // parent post - if this is not null this is a comment
        public static final String COLUMN_PARENT_ID = "parent_id";

        // did user mark this post as favorite (for different networks different actions could be
        // after user did this)
        public static final String COLUMN_FAVORITE = "favorite";

        public static Uri buildUri(String networkName, long id) {
            return CONTENT_URI.buildUpon().appendPath(networkName).appendPath(String.valueOf(id)).build();
        }

        public static String getNetworkNameFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static long getPostIdFromUri(Uri uri) {
            return ContentUris.parseId(uri);
        }

        public abstract String getInsertSpecificColumnsQuery();
    }

    public static abstract class AuthorEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_AUTHOR).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + PostContract.CONTENT_AUTHORITY + "/" + PATH_AUTHOR;

        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + PostContract.CONTENT_AUTHORITY + "/" + PATH_AUTHOR;

        public static final String TABLE_NAME = "author";

        // id of author in social network
        public static final String COLUMN_NETWORK_ID = "network_id";

        // name
        public static final String COLUMN_NAME = "name";

        // path to small picture of an author
        public static final String COLUMN_IMAGE_SMALL = "image_small";

        // path to large picture of an author
        public static final String COLUMN_IMAGE_BIG = "image_big";

        public static String getNameFromUri(Uri uri) {
            return uri.getLastPathSegment();
        }

        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public abstract String getInsertSpecificColumnsQuery();
    }

    public abstract PostEntry getPostEntry();
    public abstract AuthorEntry getAuthorEntry();
    public abstract String getName();

    @Override
    public String toString() {
        return getName();
    }
}
