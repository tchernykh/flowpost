package com.yabby.flowpost.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.yabby.flowpost.FlowPostApplication;
import com.yabby.flowpost.data.facebook.FacebookContract;
import com.yabby.flowpost.data.googleplus.GooglePlusContract;
import com.yabby.flowpost.data.twitter.TwitterContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Provide data for application from set of provider helpers which works directly with databases.
 */
public class PostProvider extends ContentProvider {

    static {
        // this could be the code that executed earlier than anything in the app, so run
        // Application initialization here
        FlowPostApplication.init();
    }

    private static final int POST = 100;
    private static final int POST_ID = 101;
    private static final int AUTHOR = 200;
    private static final int AUTHOR_ID = 201;

    private static UriMatcher sUriMatcher = buildUriMatcher();
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = PostContract.CONTENT_AUTHORITY;
        matcher.addURI(authority, PostContract.PATH_POST, POST);
        matcher.addURI(authority, PostContract.PATH_POST + "/*/#", POST_ID);
        matcher.addURI(authority, PostContract.PATH_AUTHOR, AUTHOR);
        matcher.addURI(authority, PostContract.PATH_AUTHOR + "/#", AUTHOR_ID);
        return matcher;
    }

    private List<ProviderHelper> providerHelpers;

    @Override
    public boolean onCreate() {
        providerHelpers = new ArrayList<>();
        providerHelpers.add(new ProviderHelperImpl(getContext(), new FacebookContract()));
        providerHelpers.add(new ProviderHelperImpl(getContext(), new TwitterContract()));
        providerHelpers.add(new ProviderHelperImpl(getContext(), new GooglePlusContract()));
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Timber.d("query %s", uri);
        // Here's the switch statement that, given a URI, will determine what kind of request it is,
        // and query the database accordingly.
        Cursor retCursor = null;
        switch (sUriMatcher.match(uri)) {
            // "post"
            case POST: {
                Map<Cursor, String> cursors = new HashMap<>();
                for (ProviderHelper providerHelper : providerHelpers) {
                    final Cursor postCursor = providerHelper.getPostCursor(projection,
                            selection,
                            selectionArgs,
                            sortOrder);
                    cursors.put(postCursor, providerHelper.getName());
                }
                retCursor = new SortCursor(cursors, PostContract.PostEntry.COLUMN_DATE, false);
                Timber.d("inside post");
                break;
            }
            // post/networkName/#id
            case POST_ID: {
                String networkName = PostContract.PostEntry.getNetworkNameFromUri(uri);
                for (ProviderHelper providerHelper : providerHelpers) {
                    if (networkName.equals(providerHelper.getName())) {
                        retCursor = providerHelper.getPostCursor(
                                projection,
                                PostContract.PostEntry.TABLE_NAME + "." + PostContract.PostEntry._ID + " = ?",
                                new String[]{String.valueOf(ContentUris.parseId(uri))},
                                sortOrder);
                        break;
                    }
                }
                break;
            }
            // "author"
            case AUTHOR: {
                List<Cursor> cursor = new ArrayList<>();
                for (ProviderHelper providerHelper : providerHelpers) {
                    cursor.add(providerHelper.getAuthorCursor(projection,
                            selection,
                            selectionArgs,
                            sortOrder));
                }
                retCursor = new MergeCursor(cursor.toArray(new Cursor[cursor.size()]));
                break;
            }
            case AUTHOR_ID: {
                List<Cursor> cursor = new ArrayList<>();
                for (ProviderHelper providerHelper : providerHelpers) {
                    final Cursor authorCursor = providerHelper.getAuthorCursor(
                            projection,
                            PostContract.AuthorEntry._ID + " = ?",
                            new String[]{PostContract.AuthorEntry.getNameFromUri(uri)},
                            sortOrder);
                    cursor.add(authorCursor);
                }
                retCursor = new MergeCursor(cursor.toArray(new Cursor[cursor.size()]));
                break;
            }
        }
        if (retCursor == null) {
            String networkName = PostContract.PostEntry.getNetworkNameFromUri(uri);
            for (ProviderHelper providerHelper : providerHelpers) {
                if (providerHelper.getName().equals(networkName)) {
                    retCursor = providerHelper.query(uri, projection, selection, selectionArgs, sortOrder);
                    if (retCursor != null) break;
                }
            }
        }
        if (retCursor == null) {
            Timber.d("query return is null");
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        Timber.d("query resulted %s count: %d", retCursor, retCursor.getCount());
        return retCursor;
    }

    @Override
    public String getType(Uri uri) {
        // Use the Uri Matcher to determine what kind of URI this is.
        switch (sUriMatcher.match(uri)) {
            case POST:
                return PostContract.PostEntry.CONTENT_TYPE;
            case POST_ID:
                return PostContract.PostEntry.CONTENT_ITEM_TYPE;
            case AUTHOR:
                return PostContract.AuthorEntry.CONTENT_TYPE;
            case AUTHOR_ID:
                return PostContract.AuthorEntry.CONTENT_ITEM_TYPE;
        }
        for (ProviderHelper providerHelper : providerHelpers) {
            if (providerHelper.isMatch(uri)) {
                return providerHelper.getType(uri);
            }
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        for (ProviderHelper providerHelper : providerHelpers) {
            if (providerHelper.isMatch(uri)) {
                Uri res = providerHelper.insert(uri, contentValues);
                if (res != null) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return res;
            }
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }


    @Override
    public int bulkInsert(Uri uri, @NonNull ContentValues[] values) {
        for (ProviderHelper providerHelper : providerHelpers) {
            if (providerHelper.isMatch(uri)) {
                int res = providerHelper.bulkInsert(uri, values);
                if (res >= 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return res;
            }
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    @Override
    public int delete(Uri uri, String projection, String[] projectionArgs) {
        for (ProviderHelper providerHelper : providerHelpers) {
            if (providerHelper.isMatch(uri)) {
                int res = providerHelper.delete(uri, projection, projectionArgs);
                if (res >= 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return res;
            }
        }
        // no specific delete - delete from all providerHelpers
        int affectedRows = 0;
        boolean found = false;
        for (ProviderHelper providerHelper : providerHelpers) {
            Uri specificUri = uri.buildUpon().appendPath(providerHelper.getName()).build();
            int res = providerHelper.delete(specificUri, projection, projectionArgs);
            if (res < 0) {
                continue;
            } if (res > 0) {
                affectedRows += res;
            }
            found = true;
        }
        if (affectedRows > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        if (!found) {
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return affectedRows;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String projection, String[] projectionArgs) {
        for (ProviderHelper providerHelper : providerHelpers) {
            if (providerHelper.isMatch(uri)) {
                int res = providerHelper.update(uri, contentValues, projection, projectionArgs);
                if (res >= 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return res;
            }
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }
}