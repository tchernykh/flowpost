package com.yabby.flowpost.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import com.yabby.flowpost.BuildConfig;
import com.yabby.flowpost.tools.SQLiteCursorFactory;

import timber.log.Timber;

/**
 * Implements basic database functions for a social network using it's contract passed to constructor.
 * Created by chernykh on 10/21/2014.
 */
public class NetworkDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    private final PostContract mPostContract;

    public static SQLiteDatabase.CursorFactory getCursorFactory() {
        if (BuildConfig.DEBUG) {
            return new SQLiteCursorFactory();
        }
        return null;
    }

    public NetworkDbHelper(Context context, PostContract postContract) {
        super(context, getDatabaseName(postContract), getCursorFactory(), DATABASE_VERSION);
        mPostContract = postContract;
    }

    private static String getDatabaseName(PostContract postContract) {
        return postContract.getName() + ".db";
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String additionalCols = mPostContract.getAuthorEntry().getInsertSpecificColumnsQuery();
        if (!TextUtils.isEmpty(additionalCols) && !additionalCols.endsWith(",")) {
            additionalCols += ",";
        }
        final String SQL_CREATE_AUTHOR_TABLE = "CREATE TABLE " + PostContract.AuthorEntry.TABLE_NAME + " (" +
                PostContract.AuthorEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                PostContract.AuthorEntry.COLUMN_NETWORK_ID + " TEXT NOT NULL," +
                PostContract.AuthorEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                PostContract.AuthorEntry.COLUMN_IMAGE_SMALL + " TEXT, " +
                PostContract.AuthorEntry.COLUMN_IMAGE_BIG + " TEXT," +
                additionalCols +
                " UNIQUE (" + PostContract.AuthorEntry.COLUMN_NETWORK_ID + ") ON CONFLICT REPLACE);";
        Timber.d("create: %s author: %s", mPostContract, SQL_CREATE_AUTHOR_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_AUTHOR_TABLE);

        String postAdditionalCols = mPostContract.getPostEntry().getInsertSpecificColumnsQuery();
        if (!TextUtils.isEmpty(postAdditionalCols) && !postAdditionalCols.endsWith(",")) {
            postAdditionalCols += ",";
        }
        final String SQL_CREATE_POST_TABLE = "CREATE TABLE " + PostContract.PostEntry.TABLE_NAME + " (" +
                PostContract.PostEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                PostContract.PostEntry.COLUMN_NETWORK_ID + " TEXT NOT NULL," +
                PostContract.PostEntry.COLUMN_CONTENT + " TEXT NOT NULL, " +
                PostContract.PostEntry.COLUMN_DATE + " TEXT NOT NULL, " +
                PostContract.PostEntry.COLUMN_AUTHOR_ID + " INTEGER NOT NULL, " +
                PostContract.PostEntry.COLUMN_PARENT_ID + " INTEGER," +
                PostContract.PostEntry.COLUMN_FAVORITE + " INTEGER," +
                postAdditionalCols +
                // Set up the author column as a foreign key to author table.
                " FOREIGN KEY (" + PostContract.PostEntry.COLUMN_AUTHOR_ID + ") REFERENCES " +
                PostContract.AuthorEntry.TABLE_NAME + " (" + PostContract.AuthorEntry._ID + "), " +

                " FOREIGN KEY (" + PostContract.PostEntry.COLUMN_PARENT_ID + ") REFERENCES " +
                PostContract.PostEntry.TABLE_NAME + " (" + PostContract.PostEntry._ID + "), " +

                " UNIQUE (" + PostContract.PostEntry.COLUMN_NETWORK_ID + ") ON CONFLICT REPLACE);";
        Timber.d("create: %s post: %s", mPostContract, SQL_CREATE_POST_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_POST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Timber.d("onUpgrade - drop and recreate tables for %s", mPostContract);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PostContract.PostEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PostContract.AuthorEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    @Override
    public String toString() {
        return "NetworkDbHelper(" + mPostContract.getName() + " db: " + getDatabaseName() + ")";
    }
}
