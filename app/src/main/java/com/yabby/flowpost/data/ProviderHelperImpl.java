package com.yabby.flowpost.data;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.yabby.flowpost.BuildConfig;

import timber.log.Timber;

/**
 * This is an abstract class that incapsulates logic of maintaining author-post data structure logic
 * using sqlite database.
 * Created by chernykh on 10/21/2014.
 */
public class ProviderHelperImpl implements ProviderHelper {

    private static final int POST = 1100;
    private static final int POST_ID = 1101;
    private static final int AUTHOR = 1200;
    private static final int AUTHOR_ID = 1201;

    private final SQLiteQueryBuilder mPostWithAuthorQueryBuilder;
    private final UriMatcher mUriMatcher;
    private SQLiteOpenHelper mDbHelper;
    private PostContract mPostContract;

    public ProviderHelperImpl(Context context, PostContract postContract) {
        mPostContract = postContract;
        mPostWithAuthorQueryBuilder = new SQLiteQueryBuilder();
        final String inTables = PostContract.PostEntry.TABLE_NAME + " JOIN " +
                PostContract.AuthorEntry.TABLE_NAME +
                " ON " + PostContract.PostEntry.TABLE_NAME +
                "." + PostContract.PostEntry.COLUMN_AUTHOR_ID +
                " = " + PostContract.AuthorEntry.TABLE_NAME +
                "." + PostContract.AuthorEntry._ID;
        mPostWithAuthorQueryBuilder.setTables(
                inTables
        );

        mUriMatcher = buildUriMatcher();
        mDbHelper = new NetworkDbHelper(context, postContract);
    }

    private UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = PostContract.CONTENT_AUTHORITY;
        matcher.addURI(authority, PostContract.PATH_POST + "/" + getName(), POST);
        matcher.addURI(authority, PostContract.PATH_POST + "/" + getName() + "/#", POST_ID);
        matcher.addURI(authority, PostContract.PATH_AUTHOR + "/" + getName(), AUTHOR);
        matcher.addURI(authority, PostContract.PATH_AUTHOR + "/" + getName() + "/#", AUTHOR_ID);
        return matcher;
    }

    @Override
    public boolean isMatch(Uri uri) {
        return mUriMatcher.match(uri) >= 0;
    }

    @Override
    public String getName() {
        return mPostContract.getName();
    }

    @Override
    public Cursor getPostCursor(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (selection == null) {
            selection = mPostContract.getDefaultFlowSelection();
        }
        final Cursor cursor = mPostWithAuthorQueryBuilder.query(
                mDbHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder == null ? DEFAULT_POST_ORDER : sortOrder
        );
        Timber.d("getPostCursor: %s %s %s %s %s", mDbHelper, projection, selection, selectionArgs, sortOrder);
        if (BuildConfig.DEBUG) Timber.d("getPostCursor result: %d", cursor.getCount());
        return cursor;
    }

    @Override
    public Cursor getAuthorCursor(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final Cursor cursor = mDbHelper.getReadableDatabase().query(
                PostContract.AuthorEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
        Timber.d("getAuthorCursor: %s %s %s %s %s", mDbHelper, projection, selection, selectionArgs, sortOrder);
        if (BuildConfig.DEBUG) Timber.d("getAuthorCursor result: %d", cursor.getCount());
        return cursor;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Timber.d("query %s %s %s %s %s", uri, projection, selection, selectionArgs, sortOrder);
        Cursor retCursor = null;
        switch (mUriMatcher.match(uri)) {
            // "post/${network}"
            case POST:
                if (selection == null) {
                    selection = mPostContract.getDefaultFlowSelection();
                }
                retCursor = mPostWithAuthorQueryBuilder.query(
                        mDbHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder == null ? DEFAULT_POST_ORDER : sortOrder
                );
                if (BuildConfig.DEBUG) Timber.d("query POST result: %d", retCursor == null? null: retCursor.getCount());
                break;
            // "post/*network/#"
            case POST_ID:
                retCursor = mPostWithAuthorQueryBuilder.query(
                        mDbHelper.getReadableDatabase(),
                        projection,
                        PostContract.PostEntry._ID + " = ?",
                        new String[]{String.valueOf(ContentUris.parseId(uri))},
                        null,
                        null,
                        null
                );
                if (BuildConfig.DEBUG) Timber.d("query POST_ID result: %d", retCursor == null? null: retCursor.getCount());
                break;
            // "author/${network}"
            case AUTHOR:
                retCursor = mDbHelper.getReadableDatabase().query(
                        PostContract.AuthorEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                if (BuildConfig.DEBUG) Timber.d("query AUTHOR result: %d", retCursor == null? null: retCursor.getCount());
                break;
            // "author/${network}/#"
            case AUTHOR_ID:
                retCursor = mDbHelper.getReadableDatabase().query(
                        PostContract.AuthorEntry.TABLE_NAME,
                        projection,
                        PostContract.AuthorEntry._ID + " = ?",
                        new String[]{String.valueOf(ContentUris.parseId(uri))},
                        null,
                        null,
                        null
                );
                if (BuildConfig.DEBUG) Timber.d("query AUTHOR_ID result: %d", retCursor == null? null: retCursor.getCount());
                break;
            default:
                Timber.e("query no match found: %s", uri);
                break;
        }
        return retCursor;
    }

    @Override
    public String getType(Uri uri) {
        // Use the Uri Matcher to determine what kind of URI this is.
        switch (mUriMatcher.match(uri)) {
            case POST:
                return PostContract.PostEntry.CONTENT_TYPE;
            case POST_ID:
                return PostContract.PostEntry.CONTENT_ITEM_TYPE;
            case AUTHOR:
                return PostContract.AuthorEntry.CONTENT_TYPE;
            case AUTHOR_ID:
                return PostContract.AuthorEntry.CONTENT_ITEM_TYPE;
            default:
                Timber.e("getType no match found: %s", uri);
                break;
        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        long id;
        switch (mUriMatcher.match(uri)) {
            case POST:
                id = mDbHelper.getWritableDatabase().insertOrThrow(PostContract.PostEntry.TABLE_NAME, null, contentValues);
                Timber.d("insert POST id: %d", id);
                if ( id > 0 ) return PostContract.PostEntry.buildUri(getName(), id);
            case AUTHOR:
                id = mDbHelper.getWritableDatabase().insertOrThrow(PostContract.AuthorEntry.TABLE_NAME, null, contentValues);
                Timber.d("insert AUTHOR id: %d", id);
                if ( id > 0 ) return PostContract.AuthorEntry.buildUri(id);
            default:
                Timber.e("insert no match found: %s", uri);
                break;
        }
        Timber.e("insert failed: %s", uri);
        return null;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db;
        int returnCount = -1;
        switch (mUriMatcher.match(uri)) {
            case AUTHOR:
                returnCount = 0;
                db = mDbHelper.getWritableDatabase();
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        try {
                            long _id = db.insert(PostContract.AuthorEntry.TABLE_NAME, null, value);
                            if (_id != -1) {
                                returnCount++;
                                value.put(PostContract.AuthorEntry._ID, _id);
                            }
                        } catch (Exception ex) {
                            Timber.e(ex, "Unable to insert author record to database - probably wrong data received");
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                Timber.d("bulkInsert AUTHOR to %s count: %d", mDbHelper, returnCount);
                break;
            case POST:
                returnCount = 0;
                db = mDbHelper.getWritableDatabase();
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        try {
                            long _id = db.insert(PostContract.PostEntry.TABLE_NAME, null, value);
                            if (_id != -1) {
                                returnCount++;
                                value.put(PostContract.PostEntry._ID, _id);
                            }
                        } catch (Exception ex) {
                            Timber.e(ex, "Unable to insert post record to database - probably wrong data received");
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                Timber.d("bulkInsert POST to %s count: %d", mDbHelper, returnCount);
                break;
            default:
                Timber.e("bulkInsert to %s no match found: %s", mDbHelper, uri);
                break;
        }
        return returnCount;
    }

    @Override
    public int delete(Uri uri, String projection, String[] projectionArgs) {
        switch (mUriMatcher.match(uri)) {
            case AUTHOR:
                final int deletedAuthor = mDbHelper.getWritableDatabase().delete(
                        PostContract.AuthorEntry.TABLE_NAME,
                        projection, projectionArgs);
                Timber.e("deleted AUTHOR count: %d", deletedAuthor);
                return deletedAuthor;
            case AUTHOR_ID:
                final int deletedAuthorId = mDbHelper.getWritableDatabase().delete(
                        PostContract.AuthorEntry.TABLE_NAME,
                        PostContract.AuthorEntry.COLUMN_NAME + " = " + PostContract.AuthorEntry.getNameFromUri(uri),
                        null);
                Timber.e("deleted AUTHOR_ID count: %d", deletedAuthorId);
                return deletedAuthorId;
            case POST:
                final int deletedPost = mDbHelper.getWritableDatabase().delete(
                        PostContract.PostEntry.TABLE_NAME,
                        projection, projectionArgs);
                Timber.e("deleted POST count: %d", deletedPost);
                return deletedPost;
            case POST_ID:
                final int deletedPostId = mDbHelper.getWritableDatabase().delete(
                        PostContract.PostEntry.TABLE_NAME,
                        PostContract.PostEntry._ID + " = " + ContentUris.parseId(uri), null);
                Timber.e("deleted POST_ID count: %d", deletedPostId);
                return deletedPostId;
            default:
                Timber.e("delete no match found: %s", uri);
                break;
        }
        return -1;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String projection, String[] projectionArgs) {
        switch (mUriMatcher.match(uri)) {
            case AUTHOR:
                final int updatedAuthor = mDbHelper.getWritableDatabase().update(
                        PostContract.AuthorEntry.TABLE_NAME, contentValues, projection, projectionArgs);
                Timber.e("deleted AUTHOR count: %d", updatedAuthor);
                return updatedAuthor;
            case AUTHOR_ID:
                final int updatedAuthorId = mDbHelper.getWritableDatabase().update(
                        PostContract.PostEntry.TABLE_NAME, contentValues,
                        PostContract.AuthorEntry.COLUMN_NAME + " = " + PostContract.AuthorEntry.getNameFromUri(uri),
                        projectionArgs);
                Timber.e("deleted AUTHOR_ID count: %d", updatedAuthorId);
                return updatedAuthorId;
            case POST:
                final int updatedPost = mDbHelper.getWritableDatabase().update(
                        PostContract.PostEntry.TABLE_NAME, contentValues, projection, projectionArgs);
                Timber.e("deleted POST count: %d", updatedPost);
                return updatedPost;
            case POST_ID:
                final int updatedPostId = mDbHelper.getWritableDatabase().update(
                        PostContract.PostEntry.TABLE_NAME, contentValues,
                        PostContract.PostEntry._ID + " = " + ContentUris.parseId(uri), null);
                Timber.e("deleted POST_ID count: %d", updatedPostId);
                return updatedPostId;
            default:
                Timber.e("update no match found: %s", uri);
                break;
        }
        return -1;
    }

    @Override
    public String toString() {
        return "ProviderHelperImpl(" + getName() + ")";
    }
}