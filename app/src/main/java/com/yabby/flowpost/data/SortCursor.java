package com.yabby.flowpost.data;

import android.database.AbstractCursor;
import android.database.Cursor;
import android.database.DataSetObserver;

import java.util.Map;
import java.util.Set;

import timber.log.Timber;

/**
 * A variant of MergeCursor that sorts the cursors being merged. If decent
 * performance is ever obtained, it can be put back under android.database.
 * TODO make test for this class
 */
public class SortCursor extends AbstractCursor{
    public static final int CURSOR_NAME_INDEX = Integer.MIN_VALUE;
    public static final String CURSOR_NAME_KEY = "cursor_name";
    private Cursor mCursor; // updated in onMove
    private Cursor[] mCursors;
    private Map<Cursor, String> mCursorsToNameMap;
    private int [] mSortColumns;
    private final int ROWCACHESIZE = 64;
    private int mRowNumCache[] = new int[ROWCACHESIZE];
    private int mCursorCache[] = new int[ROWCACHESIZE];
    private int mCurRowNumCache[][];
    private int mLastCacheHit = -1;
    private final boolean mAsc;

    public SortCursor(Map<Cursor, String> cursorsToNameMap, String sortColumn, boolean asc) {
        mCursorsToNameMap = cursorsToNameMap;
        Set<Cursor> cursorSet = cursorsToNameMap.keySet();
        mCursors = cursorSet.toArray(new Cursor[cursorSet.size()]);
        mAsc = asc;

        int length = mCursors.length;
        mSortColumns = new int[length];
        for (int i = 0 ; i < length ; i++) {
            if (mCursors[i] == null) continue;

            // Register ourself as a data set observer
            mCursors[i].registerDataSetObserver(new DataSetObserver() {
                @Override
                public void onChanged() {
                    // Reset our position so the optimizations in move-related code
                    // don't screw us over
                    mPos = -1;
                }

                @Override
                public void onInvalidated() {
                    mPos = -1;
                }
            });

            mCursors[i].moveToFirst();

            // We don't catch the exception
            mSortColumns[i] = mCursors[i].getColumnIndexOrThrow(sortColumn);
        }
        mCursor = null;
        String smallest = "";
        for (int j = 0 ; j < length; j++) {
            if (mCursors[j] == null || mCursors[j].isAfterLast())
                continue;
            String current = mCursors[j].getString(mSortColumns[j]);
            boolean b = current.compareToIgnoreCase(smallest) < 0;
            if (!mAsc) b = !b;
            if (mCursor == null || b) {
                smallest = current;
                mCursor = mCursors[j];
            }
        }

        for (int i = mRowNumCache.length - 1; i >= 0; i--) {
            mRowNumCache[i] = -2;
        }
        mCurRowNumCache = new int[ROWCACHESIZE][length];
    }

    @Override
    public int getCount() {
        int count = 0;
        for (Cursor cursor : mCursors) {
            if (cursor != null) {
                count += cursor.getCount();
            }
        }
        return count;
    }

    @Override
    public boolean onMove(int oldPosition, int newPosition) {
        if (oldPosition == newPosition)
            return true;

        /* Find the right cursor
         * Because the client of this cursor (the listadapter/view) tends
         * to jump around in the cursor somewhat, a simple cache strategy
         * is used to avoid having to search all cursors from the start.
         * TODO: investigate strategies for optimizing random access and
         * reverse-order access.
         */

        int cache_entry = newPosition % ROWCACHESIZE;

        if (mRowNumCache[cache_entry] == newPosition) {
            int which = mCursorCache[cache_entry];
            mCursor = mCursors[which];
            if (mCursor == null) {
                Timber.w("onMove: cache results in a null cursor.");
                return false;
            }
            mCursor.moveToPosition(mCurRowNumCache[cache_entry][which]);
            mLastCacheHit = cache_entry;
            return true;
        }

        mCursor = null;
        int length = mCursors.length;

        if (mLastCacheHit >= 0) {
            for (int i = 0; i < length; i++) {
                if (mCursors[i] == null) continue;
                mCursors[i].moveToPosition(mCurRowNumCache[mLastCacheHit][i]);
            }
        }

        if (newPosition < oldPosition || oldPosition == -1) {
            for (Cursor cursor : mCursors) {
                if (cursor != null) cursor.moveToFirst();
            }
            oldPosition = 0;
        }
        if (oldPosition < 0) {
            oldPosition = 0;
        }

        // search forward to the new position
        int smallestIdx = -1;
        for(int i = oldPosition; i <= newPosition; i++) {
            String smallest = "";
            smallestIdx = -1;
            for (int j = 0 ; j < length; j++) {
                if (mCursors[j] == null || mCursors[j].isAfterLast()) {
                    continue;
                }
                String current = mCursors[j].getString(mSortColumns[j]);
                boolean b = current.compareToIgnoreCase(smallest) < 0;
                if (!mAsc) b = !b;
                if (smallestIdx < 0 || b) {
                    smallest = current;
                    smallestIdx = j;
                }
            }
            if (i == newPosition) break;
            if (mCursors[smallestIdx] != null) {
                mCursors[smallestIdx].moveToNext();
            }
        }
        mCursor = mCursors[smallestIdx];
        mRowNumCache[cache_entry] = newPosition;
        mCursorCache[cache_entry] = smallestIdx;
        for (int i = 0; i < length; i++) {
            if (mCursors[i] != null) {
                mCurRowNumCache[cache_entry][i] = mCursors[i].getPosition();
            }
        }
        mLastCacheHit = -1;
        return true;
    }

    @Override
    public int getColumnIndex(String columnName) {
        if (CURSOR_NAME_KEY.equals(columnName)) return CURSOR_NAME_INDEX;
        return super.getColumnIndex(columnName);
    }

    @Override
    public String getString(int column) {
        if (SortCursor.CURSOR_NAME_INDEX == column) return mCursorsToNameMap.get(mCursor);
        return mCursor.getString(column);
    }

    @Override
    public short getShort(int column) {
        return mCursor.getShort(column);
    }

    @Override
    public int getInt(int column) {
        return mCursor.getInt(column);
    }

    @Override
    public long getLong(int column) {
        return mCursor.getLong(column);
    }

    @Override
    public float getFloat(int column) {
        return mCursor.getFloat(column);
    }

    @Override
    public double getDouble(int column) {
        return mCursor.getDouble(column);
    }

    @Override
    public boolean isNull(int column) {
        return mCursor.isNull(column);
    }

    @Override
    public byte[] getBlob(int column) {
        return mCursor.getBlob(column);
    }

    @Override
    public String[] getColumnNames() {
        if (mCursor != null) {
            return mCursor.getColumnNames();
        } else {
            // All of the cursors may be empty, but they can still return this information.
            for (Cursor cursor : mCursors) {
                if (cursor != null) {
                    return cursor.getColumnNames();
                }
            }
            throw new IllegalStateException("No cursor that can return names");
        }
    }

    @Override
    public void deactivate() {
        for (Cursor cursor : mCursors) {
            if (cursor != null) cursor.deactivate();
        }
    }

    @Override
    public void close() {
        for (Cursor cursor : mCursors) {
            if (cursor != null) cursor.close();
        }
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        for (Cursor cursor : mCursors) {
            if (cursor != null) cursor.registerDataSetObserver(observer);
        }
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        for (Cursor cursor : mCursors) {
            if (cursor != null) cursor.unregisterDataSetObserver(observer);
        }
    }

    @Override
    public boolean requery() {
        for (Cursor cursor : mCursors) {
            if (cursor != null) {
                if (!cursor.requery()) return false;
            }
        }
        return true;
    }
}