package com.yabby.flowpost.data.twitter;

import android.net.Uri;

import com.yabby.flowpost.data.PostContract;

/**
 * Contract for twitter data
 */
public class TwitterContract extends PostContract {

    public static final String TWITTER = "twitter";

    @Override
    public PostContract.PostEntry getPostEntry() {
        return new PostEntry();
    }

    @Override
    public PostContract.AuthorEntry getAuthorEntry() {
        return new AuthorEntry();
    }

    @Override
    public String getName() {
        return TWITTER;
    }

    public static final class PostEntry extends PostContract.PostEntry {
        public static final Uri CONTENT_URI = PostContract.PostEntry.CONTENT_URI.buildUpon().appendPath(TWITTER).build();

        public static final String COLUMN_MEDIA = "media_list";

        public static final String COLUMN_RETWEET_COUNT = "retweet_count";

        public static final String COLUMN_FAVORITE_COUNT = "favorite_count";

        // original author of the post if post is a retweet
        public static final String COLUMN_ORIGINAL_AUTHOR_ID = "original_author";

        public String getInsertSpecificColumnsQuery() {
            return COLUMN_MEDIA + " BLOB," +
                   COLUMN_RETWEET_COUNT + " INTEGER," +
                   COLUMN_FAVORITE_COUNT + " INTEGER," +
                   COLUMN_ORIGINAL_AUTHOR_ID + " INTEGER, " +

                   // Set up the author column as a foreign key to author table.
                   " FOREIGN KEY (" + COLUMN_ORIGINAL_AUTHOR_ID + ") REFERENCES " +
                   PostContract.AuthorEntry.TABLE_NAME + " (" + PostContract.AuthorEntry._ID + ")";
        }
    }

    public static final class AuthorEntry extends PostContract.AuthorEntry {
        public static final Uri CONTENT_URI = PostContract.AuthorEntry.CONTENT_URI.buildUpon().appendPath(TWITTER).build();

        public static final String COLUMN_SCREEN_NAME = "screen_name";

        public String getInsertSpecificColumnsQuery() {
            return COLUMN_SCREEN_NAME + " TEXT NOT NULL";
        }
    }

}
