package com.yabby.flowpost;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.yabby.flowpost.data.facebook.FacebookContract;
import com.yabby.flowpost.data.googleplus.GooglePlusContract;
import com.yabby.flowpost.data.twitter.TwitterContract;
import com.yabby.flowpost.networks.FacebookConnector;
import com.yabby.flowpost.networks.GooglePlusConnector;
import com.yabby.flowpost.networks.TwitterConnector;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * A placeholder fragment containing a simple view.
 */
public class ConfigureNetworksFragment extends BaseFragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private NetworksListAdapter mNetworksListAdapter;

    public static ConfigureNetworksFragment newInstance() {
        return new ConfigureNetworksFragment();
    }

    public ConfigureNetworksFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_configure_networks, container, false);
        ListView mNetworksList = (ListView) rootView.findViewById(R.id.listview_networks);
        List<String> networks = new ArrayList<>();
        networks.add(FacebookContract.FACEBOOK);
        networks.add(TwitterContract.TWITTER);
        networks.add(GooglePlusContract.GOOGLEPLUS);
        mNetworksListAdapter = new NetworksListAdapter(getActivity(), 0, networks);
        mNetworksList.setAdapter(mNetworksListAdapter);
        mNetworksList.setOnItemClickListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mNetworksListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onClick(View v) {
        String networkName = (String) v.getTag();
        switch (networkName) {
            case FacebookContract.FACEBOOK:
                Account facebookAccount = FacebookConnector.getInstance().getSyncAccount(getActivity());
                if (facebookAccount == null) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            final AccountManager accountMgr = AccountManager.get(getActivity());
                            accountMgr.addAccount(getString(R.string.sync_account_type_facebook), "", null, null, getActivity(), null, null);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            mNetworksListAdapter.notifyDataSetChanged();
                        }
                    }.execute();
                } else {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            FacebookConnector.getInstance().removeSyncAccount(getActivity());
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            mNetworksListAdapter.notifyDataSetChanged();
                        }
                    }.execute();
                }
                break;
            case TwitterContract.TWITTER:
                Account twitterAccount = TwitterConnector.getInstance().getSyncAccount(getActivity());
                if (twitterAccount == null) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            final AccountManager accountMgr = AccountManager.get(getActivity());
                            accountMgr.addAccount(getString(R.string.sync_account_type_twitter), "", null, null, getActivity(), null, null);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            mNetworksListAdapter.notifyDataSetChanged();
                        }
                    }.execute();
                } else {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            TwitterConnector.getInstance().removeSyncAccount(getActivity());
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            mNetworksListAdapter.notifyDataSetChanged();
                        }
                    }.execute();
                }
                break;
            case GooglePlusContract.GOOGLEPLUS:
                Account googleplusAccount = GooglePlusConnector.getInstance().getSyncAccount(getActivity());
                if (googleplusAccount == null) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            final AccountManager accountMgr = AccountManager.get(getActivity());
                            accountMgr.addAccount(getString(R.string.sync_account_type_googleplus), "", null, null, getActivity(), null, null);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            mNetworksListAdapter.notifyDataSetChanged();
                        }
                    }.execute();
                } else {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            GooglePlusConnector.getInstance().removeSyncAccount(getActivity());
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            mNetworksListAdapter.notifyDataSetChanged();
                        }
                    }.execute();
                }
                break;
        }
    }

    private class NetworksListAdapter extends ArrayAdapter<String> {
        public NetworksListAdapter(Context context, int resource, List<String> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.configure_network_item, parent, false);
            }

            String networkName = getItem(position);
            if (networkName!= null) {
                Button loginButton = (Button) view.findViewById(R.id.network_login_button);
                loginButton.setTag(networkName);
                switch (networkName) {
                    case FacebookContract.FACEBOOK:
                        loginButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.facebook_icon, 0, 0, 0);
                        Account facebookAccount = FacebookConnector.getInstance().getSyncAccount(getContext());
                        Timber.i("facebook account: %s", facebookAccount);
                        if (facebookAccount == null) {
                            loginButton.setText("Login to " + networkName);
                        } else {
                            loginButton.setText("Logout from " + networkName);
                        }
                        break;
                    case TwitterContract.TWITTER:
                        loginButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twitter_icon, 0, 0, 0);
                        Account twitterAccount = TwitterConnector.getInstance().getSyncAccount(getContext());
                        Timber.i("twitter account: %s", twitterAccount);
                        if (twitterAccount == null) {
                            loginButton.setText("Login to " + networkName);
                        } else {
                            loginButton.setText("Logout from " + networkName);
                        }
                        break;
                    case GooglePlusContract.GOOGLEPLUS:
                        loginButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.googleplus_icon, 0, 0, 0);
                        Account googleplusAccount = GooglePlusConnector.getInstance().getSyncAccount(getContext());
                        Timber.i("googleplus account: %s", googleplusAccount);
                        if (googleplusAccount == null) {
                            loginButton.setText("Login to " + networkName);
                        } else {
                            loginButton.setText("Logout from " + networkName);
                        }
                        break;
                }
                loginButton.setOnClickListener(ConfigureNetworksFragment.this);
            }
            return view;
        }
    }
}
